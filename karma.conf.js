"use strict";

// Configure Karma!
module.exports = function(config) {
  config.set({
    basePath: './',
    frameworks: ['mocha', 'chai', 'sinon-chai'],
    files: [
      {pattern: 'dist/libs.js', watched: false}, // libs
      'dist/tests.js'                            // all the tests + deps
    ],
    reporters: ['mocha'],
    autoWatch: true,
    autoWatchBatchDelay: 2000,
    usePolling: true
  });
};
