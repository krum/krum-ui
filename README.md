
#Krum UI

Krum UI is a web based user interface for [Krum Server]. It is built and maintained separately to the server component so that it can improve independently and use the most appropriate tools for it's construction, as opposed to krum servers API focus.

##Installation

If you're running Krum server and looking to install the Web UI please run:

        $ krum webui install

On the command line on the computer where are have Krum Server installed. If you'd like to do some development, keep reading!

##Development

Krum UI uses tools built within the [Node JS] ecosystem for it's development.

To get a local repo ready for development:

1. Install [Node JS]
2. clone the repo:

        $ git clone https://bitbucket.org/krum/krum-ui.git
        $ cd krum-ui

3. Install the node and bower dependencies:

        $ node install
        $ bower install

4. Run the gulp watch process:

        $ gulp

5. Open a browser to [http://localhost:9000]

6. Start editing and watch gulp auto-build and auto-refresh your UI!

###Running Tests

This is not currently set up using the new pipeline and tools.

##Submitting changes

Just fork the main repo on bitbucket or github and submit pull requests to get your changes incorporated. We may not take all changes, but we'll take improvements/bugfixes/etc that line up with our, admitedly currenlty undocumented, standards.


 [Krum Server]: https://bitbucket.com/krum/krum_server/
 [Node JS]: http://nodejs.org/
