/** @jsx React.DOM */

/**
 * Convenience event cancel func.
 */
function cancelEv(e) {
  // Apparently you need to do both for dragover and drop
  // Not according to docs, but according to Chrome :)
  if (e.preventDefault) {
    e.preventDefault();
  }
  if (e.stopPropagation) {
    e.stopPropagation();
  }
  return false;
}

/**
 * Provides a convenient way to bind for drop events and have the
 * bindings automatically cleaned up if the component is removed.
 */
var FileDropTargetMixin = {
  componentWillMount() {
    this._target = null;
    this._dropCallback = null;
  },
  componentWillUnmount() {
    this.removeDropTarget();
  },
  /**
   * Clear out the drop event handling gunk.
   *
   * Note: This will happen automatically when the component is
   * unmounted.
   */
  removeDropTarget() {
    if (this._target !== null) {
      this._target.removeEventListener('dragover', this._onDragOver);
      this._target.removeEventListener('drop', this._onDrop);
      this._target = null;
    }
    this._dropCallback = null;
  },
  /**
   * Bind the drop handling to element, and set up to call onDrop when
   * one or more files is dropped on that element.
   *
   * Args:
   *  element (DOMElement) : The element to be watched for file drop
   *    events.
   *  onDrop (func) : A function that will be given the file drop
   *    event. Should return false (like an old-school event handler)
   *    if it wants to keep the drop event for itself, and
   *    preventDefault/stopPropagation/etc will be called to stop the
   *    the event bubbling any further up the chain.
   */
  setDropTarget(element, onDrop) {
    this.removeDropTarget();
    this._dropCallback = onDrop;
    this._target = element;
    this._target.addEventListener('dragover', this._onDragOver);
    this._target.addEventListener('drop', this._onDrop);
  },
  _onDragOver(e) {
    // check if there are any files in there
    var isFiles = false;
    var types = e.dataTransfer.types;
    for (var i in types) {
      if (types[i].toLowerCase() === 'files') {
        isFiles = true;
        break;
      }
    }

    if (!isFiles) {
      return;
    }

    // we are accepting the transfer, so cancel further handling
    return cancelEv(e);
  },
  _onDrop(e) {
    try {
      this._dropCallback(e);
    }
    catch (e) {
      console.trace(e);
    }

    // Don't navigate, etc...
    return cancelEv(e);
  }
};

module.exports = FileDropTargetMixin;
