/** @jsx React.DOM */

var React = require('react');
var ramda = require('ramda');

var MB = Math.pow(2,20);

var UploadItem = React.createClass({
  render: function() {
    var badgeText = this.props.status;
    if (this.props.status === 'uploading') {
      badgeText = Math.floor(this.props.percent*100)+'% @'+Math.floor(this.props.speed/MB)+'MB/s';
    }

    return (
      <div className="upload-item">
        <span className="upload-label">{this.props.text}</span>
        <span className="badge">{badgeText}</span>
      </div>
    );
  }
});

var UploadQueue = React.createClass({
  render: function() {
    // we want reveresed order
    var uploads = ramda.reverse(this.props.uploads);
    return (
      <div id="upload-queue">
        <strong>Uploads:</strong>
        {uploads.map(function (u, i) {
          return (<UploadItem key={i}
                    text={u.getName()}
                    status={u.getStatus()}
                    percent={u.getCompletedPercent()}
                    speed={u.getTFerRate()}
                  />);
        })}
      </div>
    );
  }
});

module.exports = UploadQueue;
