/** @jsx React.DOM */

var React = require('react');
var StoreSliceMixin = require('../util/storeslicemixin');
var playbackSessionActions = require('../actions/playbacksessionactions');
var util = require('../util');

// Seek 5 seconds forward/backward
var SEEK_AMOUNT = 15.0;

/**
 * A simple button that shows a dropdown menu of options when pressed.
 *
 * The passed children are rendered as the dropdown buttons children.
 *
 * Props:
 *  options {[[val, label]|string|null]} - A list of 2-tuples. The val
 *    will be provided in the krumData attribute of the change event.
 *    The label is rendered as the child of the <li><a> element in the
 *    dropdown list. This means the label can be text or any React
 *    component. The val can be any javascript value, it is held and
 *    returned un-changed.
 *    strings and null can also be within the list. A string is turned
 *    into a header and null a horizonal bar.
 *  onChange {func} - Called with a single argument, the event, when
 *    an option is selected from the dropdown.
 *  [id] {string} - Used as the id on the button (for custom syling
 *    and such). If not provided, no ID is used.
 *  [alignRight] {bool} - If true, the dropdown menus right edge will
 *    be aligned to the right edge of the button, instead of the
 *    default left to left.
 */
var DropdownButton = React.createClass({
  propTypes: {
    options: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired,
    id: React.PropTypes.string,
    alignRight: React.PropTypes.bool
  },
  getDefaultProps() {
    return {
      options: [],
      onChange: () => {},
      id: '',
      alignRight: false
    };
  },
  getInitialState() {
    return {
      open: false
    };
  },
  _onOpenToggle() {
    this.setState({open: !this.state.open});
  },
  _onOptionSelected(e, val) {
    e.preventDefault();
    e.krumData = {value: val};
    this.setState({open: false});
    this.props.onChange(e);
  },
  render() {
    var containerClasses = 'dropdown';
    var menuClasses = 'dropdown-menu';
    var options=this.props.options;

    // is the dropdown menu open?
    if (this.state.open === true) {
      containerClasses += ' open';
    }
    // are we right aligned?
    if (this.props.alignRight === true) {
      menuClasses += ' dropdown-menu-right';
    }

    return (
      <span className={containerClasses}>
        <button type="button" id={this.props.id} onClick={this._onOpenToggle}>
          {this.props.children}
        </button>
        <ul className={menuClasses} role="menu">
        {options.map((opt, i) => {
          // if spacer, or header or option
          if (opt === null) {
            return (<li role="presentation" class="divider"></li>);
          }
          else if (typeof(opt) === 'string') {
            return (<li role="presentation" class="dropdown-header">{opt}</li>);
          }
          else {
            var val = opt[0];
            return (
            <li key={i} role="presentation">
              <a role="menuitem" href="#"
                 onClick={(e) => {this._onOptionSelected(e, val);}}>
                {opt[1]}
              </a>
            </li>
            );
          }
        })}
        </ul>
      </span>
    );
  }
});

/**
 * A simple volume control.
 *
 * Calls onChange with the raw event that triggered the volume change
 * and the new value in event.krumData.value;
 */
var VolumeControl = React.createClass({
  propTypes: {
    volume: React.PropTypes.number.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  getInitialState() {
    return {
      oldVolume: this.props.volume
    };
  },
  _onToggleMute(e) {
    e.preventDefault();
    if (this.props.volume === 0) {
      e.krumData = {value: this.state.oldVolume};
    }
    else {
      this.setState({oldVolume: this.props.volume});
      e.krumData = {value: 0};
    }
    this.props.onChange(e);
  },
  _onVolumeChange(e) {
    e.preventDefault();
    e.krumData = {value: e.target.value};
    this.props.onChange(e);
  },
  render() {
    var vol = this.props.volume;
    var volIcon = 'ki ki-volume-up';
    if (vol === 0) {
      volIcon = 'ki ki-volume-off';
    }
    else if (vol <= 0.5) {
      volIcon = 'ki ki-volume-down';
    }
    return (
      <label id="playcontrols-volume">
        <input type="range" className="krum-slider-v"
               min="0" max="1" step=".05" ref="slider"
               value={vol} onChange={this._onVolumeChange} />
        <button className={volIcon} onClick={this._onToggleMute}></button>
      </label>
    );
  },
  _firefoxFixSlider() {
    // Fix for Firefox to set orient=vertical because React doesn't
    this.refs.slider.getDOMNode().setAttribute('orient', 'vertical');
  },
  componentDidMount() {
    this._firefoxFixSlider();
  },
  componentDidUpdate() {
    this._firefoxFixSlider();
  }
});

/**
 * A Position slider for the content.
 */
var PositionSlider = React.createClass({
  propTypes: {
    duration: React.PropTypes.number.isRequired,
    position: React.PropTypes.number.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  _onChange(e) {
    // Don't want changes when we don't actually know the details
    if (this.props.duration === null) {
      return;
    }

    this.props.onChange(e.target.value);
  },
  render() {
    var dur = this.props.duration || 0;
    var pos = this.props.position || 0;

    return (
      <div id="playprogress">
        <input type="range" className="krum-slider" min="0" step="1"
               max={dur} value={pos} onChange={this._onChange} />
      </div>
    );
  }
});

/**
 * Provides an Icon and dropdown menu for selecting the remote player.
 *
 * When a selection is made, will call the onChange function with the
 * raw event. The event will have a krumData attribute with a value
 * attribute that is the pathname of the requested player.
 *
 * Props:
 *  players: An array of player resources for the user to choose from.
 *  onChange: Called when a player is selected. Player path available
 *  in the krumData.value attribute.
 */
var PlayerSelector = React.createClass({
  propTypes: {
    players: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  render() {
    // Build up options set
    var options = this.props.players.map((player) => {
      return [player.path, player.name];
    });

    return (
      <DropdownButton options={options} alignRight={true}
                      onChange={this.props.onChange}>
        <i className="ki ki-player-select"></i>
      </DropdownButton>
    );
  }
});

/**
 * Play/pause button that calls onChange when clicked and displays
 * a Play arrow when paused and a pause arrow when playing.
 */
var PlayPauseButton = React.createClass({
  propTypes: {
    paused: React.PropTypes.bool.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  render() {
    var playPauseClasses = 'ki ki-pause';
    var title = 'Currently playing. Click to pause.';
    if (this.props.paused === true) {
      playPauseClasses = 'ki ki-play';
      title = 'Currently paused. Click to resume playback.';
    }

    return (
      <button className={playPauseClasses} title={title}
              onClick={this.props.onChange}></button>
    );
  }
});

/**
 * Renders a seek button for either forward or backward seeking.
 */
var SeekButton = React.createClass({
  propTypes: {
    forward: React.PropTypes.bool.isRequired,
    amount: React.PropTypes.number.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  change() {
    this.props.onChange(this.props.forward, this.props.amount);
  },
  render() {
    var direction = this.props.forward ? 'forward' : 'back';
    var classes = 'ki ki-seek'+direction;
    var title = util.formatStr('Click to seek {} {} seconds.',
                               direction, this.props.amount);

    return (
      <button className={classes} onClick={this.change} title={title}/>
    );
  }
});

/**
 * The primary playback controls, forward, back, etc.
 */
var PlayerButtons = React.createClass({
  propTypes: {
    dispatcher: React.PropTypes.object.isRequired,
    session: React.PropTypes.object.isRequired,
    players: React.PropTypes.array.isRequired
  },
  _onPickNewSession(e) {
    e.preventDefault();

    var dispatcher = this.props.dispatcher;
    dispatcher.handleViewAction(playbackSessionActions.takeOver(null));
  },
  _sendPatch(delta) {
    var dispatcher = this.props.dispatcher;
    dispatcher.handleViewAction(playbackSessionActions.patchSession(delta));
  },
  /**
   * User selected a different player, so send of the change.
   */
  _selectPlayer(e) {
    this._sendPatch({
      player_path: e.krumData.value
    });
  },
  /**
   * User changed the volume, send of patch
   */
  _changeVolume(e) {
    this._sendPatch({
      volume: parseFloat(e.krumData.value)
    });
  },
  /**
   * User has asked to change play/pause status
   */
  _togglePause() {
    this._sendPatch({
      paused: !this.props.session.paused
    });
  },
  /**
   * Seek amount seconds, forwards if forwards === true.
   */
  _seek(forwards, amount) {
    var prefix = forwards ? '+' : '-';

    this._sendPatch({
      position: prefix + amount
    });
  },
  /**
   * Set the playback position specifically
   */
  _setPlaybackPosition(newPosition) {
    this._sendPatch({
      position: newPosition
    });
  },
  /**
   * Skip the first playlist item.
   */
  _skip() {
    var dispatcher = this.props.dispatcher;
    dispatcher.handleViewAction(playbackSessionActions.skipFirstPlaylistItem());
  },
  render() {
    if (this.props.session === null) {
      return null;
    }

    var session = this.props.session;

    return (
      <div>
        <div className="krum-simpletext">Currently controlling&nbsp;
          <a href="#" onClick={this._onPickNewSession} title="Change">
            {session.name} <i className="ki-small ki-change"></i>
          </a>
        </div>
        <div id="playcontrols">
          <SeekButton forward={false} amount={SEEK_AMOUNT} onChange={this._seek}/>
          <PlayPauseButton paused={session.paused} onChange={this._togglePause}/>
          <SeekButton forward={true} amount={SEEK_AMOUNT} onChange={this._seek}/>
          <button className="ki ki-next" onClick={this._skip}/>
          <VolumeControl volume={session.volume} onChange={this._changeVolume} />
          <PlayerSelector players={this.props.players} onChange={this._selectPlayer}/>
        </div>
        <PositionSlider duration={session.duration} position={session.position}
                        onChange={this._setPlaybackPosition} />
      </div>
    );
  }
});

/**
 * The playlist!
 */
var Playlist = React.createClass({
  propTypes: {
    dispatcher: React.PropTypes.object.isRequired,
    playlist: React.PropTypes.array.isRequired
  },
  render() {
    return (
      <table className="table table-striped">
        <tbody>
          {this.props.playlist.map((item, i) => {
            return (
          <tr key={i}>
            <td>{item.name}</td><td>{item.duration}</td>
          </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
});

var NewSessionInput = React.createClass({
  propTypes: {
    onChange: React.PropTypes.func.isRequired
  },
  getInitialState() {
    return {
      name: ''
    };
  },
  _onNameChange(e) {
    this.setState({name: e.target.value});
  },
  _onSubmit(e) {
    e.preventDefault();

    e.krumData = {sessionName: this.state.name};
    this.props.onChange(e);
  },
  render() {
    return (
      <form ref="theform" className="krum-simpletext" role="form" onSubmit={this._onSubmit}>
        <label htmlFor="sessionname">New session:</label>
        <div className="input-group">
          <input className="form-control" id="sessionname" placeholder="Enter name for new session"
                 value={this.state.value} onChange={this._onNameChange} />
          <a className="btn input-group-addon" title="Create and take control"
             onClick={this._onSubmit}>
            <i className="ki-small ki-takecontrol"></i>
          </a>
        </div>
      </form>
    );
  }
});

var SessionPicker = React.createClass({
  propTypes: {
    sessions: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  _onSessionSelect(e, path) {
    e.preventDefault();
    e.krumData = {sessionPath: path};
    this.props.onChange(e);
  },
  render() {
    return (
      <div>
        <p className="krum-simpletext">
          <strong>Existing sessions:</strong>
        </p>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Playing on</th>
              <th>Take control</th>
            </tr>
          </thead>
          <tbody>
            {this.props.sessions.map((session) => {
              var path = session.path;

              return (
            <tr key={session.id}>
              <td>{session.name}</td><td>Who knows?</td>
              <td>
                <a href="#" onClick={(e) => {this._onSessionSelect(e, path);}}>
                  <i className="ki ki-takecontrol"></i>
                </a>
              </td>
            </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
});

/**
 * Session selector
 *
 * Used if no sessions exist or none has been selected. Allows the
 * user to enter a session name or select an existing session.
 */
var SessionSelector = React.createClass({
  propTypes: {
    dispatcher: React.PropTypes.object.isRequired,
    sessions: React.PropTypes.array.isRequired
  },
  _onCreateNewSession(e) {
    var dispatcher = this.props.dispatcher;

    var name = e.krumData.sessionName;
    dispatcher.handleViewAction(playbackSessionActions.create(name, true));
  },
  _onSelectSession(e) {
    var dispatcher = this.props.dispatcher;

    var sessionPath = e.krumData.sessionPath;
    dispatcher.handleViewAction(playbackSessionActions.takeOver(sessionPath));
  },
  render() {
    return (
      <div id="sidebar">
        <NewSessionInput onChange={this._onCreateNewSession} />
        <SessionPicker sessions={this.props.sessions} onChange={this._onSelectSession} />
      </div>
    );
  }
});

/**
 * Simple set of filters for use with the MediaTable.
 *
 * Provides:
 *  - Textual filter
 *  - User choice of media_type(s)
 *  - Toggle of "show media missing content"
 */
var PlayControls = React.createClass({
  mixins: [StoreSliceMixin],

  /**
   * These are the values for display.
   * As they change, the new values are emitted via onChange.
   */
  propTypes: {
    core: React.PropTypes.object.isRequired
  },
  getInitialState() {
    return {
      controlSession: null,
      sessions: []
    };
  },
  componentWillMount() {
    // we want to know when the session info changes
    // TODO: add "watch one object" ability to slices and remove this
    // duplicate, divergent code...
    var playbackStore = this.props.core.stores.playbackSession;
    playbackStore.on(playbackStore.EVENT_CHANGE, this.updateSessionState);
    this.updateSessionState();

    // we want to know when the remote player collection changes
    this.addSlice('players', 'playbackPlayer');
  },
  componentWillUnmount() {
    // release our watch on the playback session store
    var playbackStore = this.props.core.stores.playbackSession;
    playbackStore.removeListener(playbackStore.EVENT_CHANGE, this.updateSessionState);
  },
  /**
   * Grab the relevant info from the playback Session store and set it
   * as our state.
   *
   * This triggers, and is used by, a render.
   */
  updateSessionState() {
    var playbackStore = this.props.core.stores.playbackSession;

    this.setState({
      controlSession: playbackStore.getControlSession(),
      sessions: playbackStore.getAllSessions()
    });
  },
  /**
   * If we've picked a session, render the controls;
   * otherwise, render a session maker/picker.
   */
  render() {
    var dispatcher = this.props.core.dispatcher;

    // these come from our reference to the playback session store data
    var sessions = this.state.sessions;
    var session = this.state.controlSession;
    var playlist = session ? session.playlist : [];
    // this comes from our slice of remote players
    var players = [];
    if (this.state.players !== undefined) {
      players = this.state.players.items;
    }

    if (session === null) {
      return (
        // SessionSelector creates sidebar div to workaround React return 1 component limit
        <SessionSelector dispatcher={dispatcher} sessions={sessions} />
      );
    }

    return (
      <div id="sidebar">
        <PlayerButtons dispatcher={dispatcher} session={session} players={players} />
        <Playlist dispatcher={dispatcher} playlist={playlist} />
      </div>
    );
  }
});

module.exports = PlayControls;
