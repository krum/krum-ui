/** @jsx React.DOM */

var React = require('react');

/**
 * Responsible for rendering the available choices for an AutoComplete
 * that is currently being typed into.
 *
 * Properties:
 *  choices ([{id : string, label : string}]) : The list of choices to
 *    display.
 *  selectedIndex (int) : The index of the currently selected choice.
 *  onHover (func(choice)) : A function that will be called as the
 *    user hovers over a choice.
 *  onClick (func(choice)) : A function that will be called when the
 *    user clocks on a choice.
 */
var AutoCompleteChoices = React.createClass({
  propTypes: {
    choices: React.PropTypes.arrayOf(React.PropTypes.shape({
      id: React.PropTypes.oneOfType([React.PropTypes.string,React.PropTypes.number]),
      label: React.PropTypes.string
    })).isRequired,
    selectedIndex: React.PropTypes.number.isRequired,
    onHover: React.PropTypes.func,
    onClick: React.PropTypes.func
  },
  getDefaultProps: function() {
    return {
      onHover: function() {},
      onClick: function() {},
    };
  },
  render: function() {
    // Turn the choice list into a list of LIs for rendering.
    var selectedIndex = this.props.selectedIndex;
    var choiceEles = this.props.choices.map((choice, i) => {
      var classes = (i === selectedIndex) ? "krum-autocomplete-selected" : "";
      return (
        <li className={classes} key={choice.id}
            onMouseOver={(e) => {this.props.onHover(e, choice);}}
            onClick={(e) => {this.props.onClick(e, choice);}}>
          {choice.label}
        </li>
      );
    });

    return (
      <ul className="krum-autocomplete-choices">{choiceEles}</ul>
    );
  }
});

/**
 * A simple Auto complete text input. Requires Users pick one of the
 * available options.
 *
 * Properties:
 *  choiceSource (func(term)) : A function that returns promise that
 *    resolves to a list of {id : string|int, label : string} objects
 *    that will be displayed to allow user selection.
 *  onChange (func(e)) : Called when an option is selected with the
 *    original event, click or Enter, that caused it. The selected
 *    choice is available in the krumData.choice attribute.
 *  defaultValue (string) : The default text to appear in the text
 *    input.
 *  minChars=3 (int) : The minimum number of characters before calling
 *    choiceSource for options to display.
 *  waitMillis=500 (int) : The number of milliseconds to wait after a
 *    keypress for further keypresses before calling choiceSource for
 *    options to display.
 */
var AutoComplete = React.createClass({
  propTypes: {
    choiceSource: React.PropTypes.func.isRequired,
    defaultValue: React.PropTypes.string,
    onChange: React.PropTypes.func.isRequired,
    minChars: React.PropTypes.number,
    waitMillis: React.PropTypes.number,
  },
  getDefaultProps() {
    return {
      defaultValue: '',
      minChars: 3,
      waitMillis: 500,
    };
  },
  getInitialState() {
    return {
      choices: [],          // List of 2-tuples (id, label)
      selectedId: null,     // The id of the current choice
      selectedIndex: null,  // The index of the current choice
      lookupTimeout: null   // The id of the timeout for choice lookup
    };
  },
  onInput(e) {
    if (e.type === 'keydown') {
      switch(e.key) {
        // up/down: select option
        case 'ArrowUp':
          this._adjustSelectionByIndex(-1);
          e.stopPropagation();
          break;
        case 'ArrowDown':
          this._adjustSelectionByIndex(1);
          e.stopPropagation();
          break;
        case 'Enter':
          this._clearOptions();
          e.krumData = {choice: this.state.choices[this.state.selectedIndex]};
          this.props.onChange(e);
          e.stopPropagation();
          break;
        // Esc: clear options, allow propagate
        case 'Escape':
          this._clearOptions();
          return;
      }
    }
    else {
      // it's a text change
      // if we have enough chars, schedule a choice query
      var text = e.target.value;
      if (text.length >= this.props.minChars) {
        this._scheduleOptionsUpdate(text);
      }
    }
  },
  onFocus(e) {
    this._updateOptions(e.target.value);
  },
  onBlur() {
    this._clearOptions();
  },
  _scheduleOptionsUpdate(newTerm) {
    // cancel any existing
    this._cancelscheduledUpdate();

    // schedule a new lookup
    this.state.lookupTimeout = setTimeout(() => {
      this._updateOptions(newTerm);
    }, this.props.waitMillis);
  },
  _cancelscheduledUpdate() {
    if (this.state.lookupTimeout !== null) {
      clearTimeout(this.state.lookupTimeout);
      this.state.lookupTimeout = null;
    }
  },
  _updateOptions(newTerm) {
    // clear any pending updates, because we're going now!
    this._cancelscheduledUpdate();

    // ask the choiceSource to source us some choices
    this.props.choiceSource(newTerm).then(newChoices => {
      this._updateSelection(newChoices, this.state.selectedId);
    })
    .catch(e => {
      console.trace('Error getting options', e, e.stack);
    });
  },
  _adjustSelectionByIndex(delta) {
    var currIndex = this.state.selectedIndex;
    if (currIndex === null) {
      currIndex = 0;
    }

    // now calc the new index
    var choices = this.state.choices;
    var len = choices.length;
    if (len === 0) {
      // just bail, this is silly
      return;
    }
    // why not just %? See http://javascript.about.com/od/problemsolving/a/modulobug.htm
    var newIndex = (((currIndex + delta)%len)+len)%len;
    if (isNaN(newIndex)) {
      throw "New index is NaN, what are you doing?";
    }

    this._updateSelection(undefined, choices[newIndex].id);
  },
  _updateSelection(choices, newId) {
    if (choices === undefined) {
      choices = this.state.choices;
    }
    if (newId === undefined) {
      newId = this.state.choices;
    }

    // find the new index of the old one selectedId
    var newIndex=null;
    for (var i=0; i < choices.length; ++i) {
      var choice = choices[i];
      if (choice.id === newId) {
        newIndex = i;
        break;
      }
    }
    // not there? default to top
    if (choices.length > 0 && newIndex === null) {
      newIndex = 0;
      newId = choices[0].id;
    }

    // this will schedule a redraw
    this.setState({
      choices: choices,
      selectedId: newId,
      selectedIndex: newIndex
    });
  },
  _clearOptions: function() {
    this.setState(this.getInitialState());
  },
  _renderOptions: function() {
    var choices=this.state.choices;
    if (choices.length === 0) {
      return "";
    }

    // Hover: Just change the selectedID, but we're not done yet.
    function onHover(e, choice) {
      this._updateSelection(undefined, choice.id);
    }
    // Click: Select the hovered option, close the autocomplete, send event
    function onClick(e, choice) {
      this._clearOptions();
      e.krumData = {choice};
      this.props.onChange(e);
    }

    return (
      <AutoCompleteChoices
        choices={choices}
        selectedIndex={this.state.selectedIndex}
        onHover={onHover.bind(this)}
        onClick={onClick.bind(this)} />
    );
  },
  render: function() {
    return (
      <div className='krum-autocomplete'>
        <input
          defaultValue={this.props.defaultValue}
          onKeyDown={this.onInput}
          onChange={this.onInput}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
        />
        { this._renderOptions() }
      </div>
    );
  }
});

// Do the exporting.
module.exports = AutoComplete;
