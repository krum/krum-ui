/** @jsx React.DOM */

var React = require('react');
var BProm = require('bluebird');
var ramda = require('ramda');
var components = require('../components');
var resource = require('../services/resource');
var util = require('../util');

var TextCell = React.createClass({
  render() {
    return (<td>{this.props.val}</td>);
  },
});

var HMSCell = React.createClass({
  render() {
    var val = util.secondsToHMS(this.props.val);
    return (<td>{val}</td>);
  },
});

var WatchCell = React.createClass({
  render() {
    var s = ' ';
    if (this.props.val != null) {
      var d = new Date(this.props.val);
      if (!isNaN(d.getYear())) {
        s = d.toISOString().slice(0, 10);;
      }
    }

    return (<td>{s}</td>);
  },
})

var EditableTextCell = React.createClass({
  propTypes: {
    onChange: React.PropTypes.func
  },
  getDefaultProps() {
    return {
      onChange: () => {}
    };
  },
  render() {
    return (<td>
              <input value={this.props.val} onChange={this.props.onChange} />
            </td>);
  }
});

var MetaAutocompleteCell = React.createClass({
  propTypes: {
    onChange: React.PropTypes.func,
    ac_path: React.PropTypes.string
  },
  getDefaultProps() {
    return {
      onChange: () => {},
      ac_path: '/api/1/autocomplete'
    };
  },
  _getOpts: BProm.coroutine(function*(term) {
    // default to only show 20 options, for brevity
    var filters = {
      limit: 50,
      media_type: [1,3],
      term: term
    };
    var opts = yield resource.get(undefined, filters, this.props.ac_path);

    return opts.list.map((obj) => {
      return {id: obj.id, label: obj.name+' ('+obj.year+')'};
    });
  }),
  render() {
    return (<td>
              <components.AutoComplete
                choiceSource={this._getOpts}
                defaultValue={this.props.val}
                onChange={this.props.onChange}
              />
            </td>);
  }
});

var DeleteRowCell = React.createClass({
  propTypes: {
    onChange: React.PropTypes.func
  },
  getDefaultProps() {
    return {
      onChange: () => {},
    };
  },
  render() {
    return (
      <td>
        <a href="javascript: {}" onClick={this.props.onChange} >
          <i className="ki ki-delete" />
        </a>
      </td>
    );
  }
});

var SelectTable = React.createClass({
  propTypes: {
    onChange: React.PropTypes.func
  },

  _onCellChange(e, colI, coldef, rowI, rowData) {
    // skip on if no-one's listening
    if (this.props.onChange === undefined) {
      return;
    }

    // augment the event (extend don't destroy krumData)
    e.krumData = ramda.mixin(e.krumData, {colI, coldef, rowI, rowData});
    // send it to any interested parties
    this.props.onChange(e);
    delete e.krumData;
  },

  render() {
    // make sure we only look at values
    var coldefs = ramda.values(this.props.coldefs);
    if (coldefs === null) {
      return;
    }

    // build up the column header and value functions
    var headers = coldefs.map((col, i) => {
      return (<th key={i}>{col.title || col.attr}</th>);
    });

    // Helper for turning a row of data into a list of cols (<td>s)
    var buildCols = (rowIndex, rowData) => {
      return coldefs.map((col, i) => {
        var CellType = col.cellType || TextCell;
        return (<CellType
                  key={i} row={rowData} val={rowData[col.attr]||null}
                  onChange={(e) => {
                    this._onCellChange(e, i, col, rowIndex, rowData);
                  }} />);
      }, this);
    };

    // now build up the rows
    var rawData = this.props.data;
    var rows = rawData.map((rowData, i) => {
      var id = this.props.idcol ? rowData[this.props.idcol] : i;
      return (<tr key={id}>{buildCols(i, rowData)}</tr>);
    });

    var classes = "table ";
    var tableBodyStyle = {};
    if (this.props.editable) {
      classes += "table-hover";
      tableBodyStyle.cursor = 'pointer';
    }
    else {
      classes += "table-striped";
    }

    return (
      <div>
        <table className={classes}>
         <thead><tr>{headers}</tr></thead>
         <tbody style={tableBodyStyle}>{rows}</tbody>
        </table>
      </div>
    );
  }
});

// Do the exporting.
exports.SelectTable = SelectTable;
exports.WatchCell = WatchCell;
exports.HMSCell = HMSCell;
exports.TextCell = TextCell;
exports.EditableTextCell = EditableTextCell;
exports.DeleteRowCell = DeleteRowCell;
exports.MetaAutocompleteCell = MetaAutocompleteCell;
