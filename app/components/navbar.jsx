/** @jsx React.DOM */

var React = require('react');

/**
 * A simple NavItem class
 */
var NavItem = React.createClass({
  render() {
    var classes = this.props.active ? "active" : "";
    return (
      <li className={classes}>
        <a href={'#'+this.props.path}>{this.props.children}</a>
      </li>
    );
  }
});

/**
 * The NavBar class.
 */
var NavBar = React.createClass({
  propTypes: {
    items: React.PropTypes.arrayOf(React.PropTypes.shape({
      href: React.PropTypes.string,
      name: React.PropTypes.string
    })).isRequired,
    active: React.PropTypes.string
  },
  render() {
    if (this.props.items.length < 1) {
      return null;
    }

    // split out the main(1st) from the rest
    var navitems = this.props.items.map((nav, i) => {
      var active = nav.path===this.props.active;

      return <NavItem key={i} path={nav.path} active={active} >{nav.text}</NavItem>;
    });

    // There's a whole lot of bootstrap crud here...
    return (
      <nav id="page-menu">
       <ul className="nav">
        {navitems}
       </ul>
      </nav>
    );
  }
});

module.exports = NavBar;
