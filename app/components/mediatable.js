/** @jsx React.DOM */

var React = require('react');
var table = require('./selecttable');
var util = require('../util');
var playbackSessionActions = require('../actions/playbacksessionactions');

var ACTION_ADDITEM = 'addItem';

/**
 * Small icon that means "download the file for this media"
 */
var MediaActionDownload = React.createClass({

  propTypes: {
    row: React.PropTypes.object.isRequired,
  },

  render() {
    var download_url, classes, title;

    var content = this.props.row.content;
    if (content.length > 0) {
      download_url = util.pathToURL(content[0]) + '?download=1';
      classes = 'ki ki-download';
      title = 'Download media file';
    }
    else {
      download_url = 'javascript: {}';
      classes = 'ki ki-nodownload';
      title = 'No file to download';
    }
    return (
      <a href={download_url} className="krum-action" title={title}>
        <i className={classes}></i>
      </a>
    );
  }
});

/**
 * Small icon that means "add this media to my playlist"
 */
var MediaActionAddToPlaylist = React.createClass({
  propTypes: {
    row: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  _onAddItem(e) {
    e.preventDefault();
    e.krumData = {action: ACTION_ADDITEM};
    this.props.onChange(e);
  },
  render() {
    var content = this.props.row.content;
    if (content.length > 0) {
      return (
        <a href="#addtoplaylist" className="krum-action"
           onClick={this._onAddItem} title="Add to playlist">
          <i className="ki ki-add-item"></i>
        </a>
      );
    }
    else {
      return (
        <a href="javascript: {}" className="krum-action" title="No file to play">
          <i className="ki ki-nodownload"></i>
        </a>
      );
    }
  }
});

/**
 * Cell that contains common Media actions, i.e. things that you
 * want to do with a single media item.
 */
var ActionCell = React.createClass({
  propTypes: {
    row: React.PropTypes.object.isRequired,
    val: React.PropTypes.any.isRequired,
    onChange: React.PropTypes.func.isRequired
  },
  render() {
    return (
      <td>
        <MediaActionAddToPlaylist row={this.props.row} onChange={this.props.onChange}/>
      </td>
    );
  }
});

// How do we want to display the data
var COLDEF = {
  NAME: {
    attr: 'name',
    title: 'Name'
  },
  LASTWATCH: {
    attr: 'lastwatched',
    title: 'Watched',
    cellType: table.WatchCell
  },
  ACTIONS: {
    attr: 'path',
    title: 'Actions',
    cellType: ActionCell
  }
};

/**
 * A table that takes a Media resource slice and draws up a table to
 * view them and act on them (download/playlist/etc).
 */
var MediaTable = React.createClass({
  propTypes: {
    data: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    dispatcher: React.PropTypes.object.isRequired
  },

  _onAction(e) {
    var dispatcher = this.props.dispatcher;

    var kd = e.krumData;
    switch (kd.action) {
      case ACTION_ADDITEM:
        var path = kd.rowData.path;
        dispatcher.handleViewAction(playbackSessionActions.appendPlaylistItem(path));
      break;

      default:
      break;
    }
  },

  render() {
    return (
      <table.SelectTable
        coldefs={COLDEF}
        idcol={'id'}
        data={this.props.data}
        onChange={this._onAction} />
    );
  }
});

module.exports = MediaTable;
