/** @jsx React.DOM */

var React = require('react');
var ramda = require('ramda');
var util = require('../util');

// Handy constants
var MT_MOVIES   = '1';
var MT_EPISODES = '2';
var MT_BOTH     = '3';
var MEDIATYPE_VALUES = {
  "1": [1],   // movies
  "2": [3],   // episodes
  "3": [1,3]  // both
};

/**
 * Helper function to ensure provided initialMediaTypes is valid for
 * the control.
 */
function checkMediaTypes(props, propName) {
  var mts = props[propName];

  if (!Array.isArray(mts) || mts.length === 0) {
    return new Error(
      util.formatStr('{} must be an array of 1, 3 or both', propName)
    );
  }

  // check that the values are what we want
  var isClean = ramda.reduce((a, x) => {
    return (a && (x === 1 || x === 3));
  }, true, mts);
  var umts = ramda.uniq(mts);
  if (!isClean || mts.length !== umts.length) {
    return Error(
      util.formatStr('{} must only contain 1 or 3 or both', propName)
    );
  }
}

/**
 * Simple set of filters for use with the MediaTable.
 *
 * Provides:
 *  - Textual filter
 *  - User choice of media_type(s)
 *  - Toggle of "show media missing content"
 */
var MediaFilters = React.createClass({
  /**
   * These are the values for display.
   * As they change, the new values are emitted via onChange.
   */
  propTypes: {
    searchTerm: React.PropTypes.string,
    mediaTypes: checkMediaTypes,
    showMissing: React.PropTypes.bool,
    onChange: React.PropTypes.func
  },
  getDefaultProps() {
    return {
      searchTerm: '',
      mediaTypes: [1, 3],
      showMissing: false,
      onChange: () => {}
    };
  },
  searchTermChange(e) {
    this.props.onChange({
      searchTerm: e.target.value
    });
  },
  mediaTypeChange(e) {
    this.props.onChange({
      mediaTypes: MEDIATYPE_VALUES[e.target.value]
    });
  },
  /**
   * Toggle the value.
   */
  showMissingChange() {
    this.props.onChange({
      showMissing: !this.props.showMissing
    });
  },
  formSubmit(e) {
    // nice, consistent React events :)
    e.preventDefault();
    e.stopPropagation();
  },
  render() {
    // make radio options
    var radioCurr = this.props.mediaTypes;
    var radios = [
      [MT_MOVIES,   ' Movies'],
      [MT_EPISODES, ' Episodes'],
      [MT_BOTH,     ' Both'],
    ].map((x) => {
      var valStr = x[0];
      var valObj = MEDIATYPE_VALUES[valStr];
      var text = x[1];
      var checked=util.eqArrayShallowNoOrder(radioCurr, valObj);

      return (
        <label key={valStr}>
          <input type="radio" name="mtypes" value={valStr} checked={checked}
            onChange={this.mediaTypeChange} />
          {text}
        </label>
      );
    }, this);

    return (
      <form onSubmit={this.formSubmit}>
        <div className="krum-mediasearch-search">
          <input type="search" placeholder="Search media"
                 onChange={this.searchTermChange}
                 onKeydown={this.searchTermChange} />
        </div>
        <div className="krum-mediasearch-options">
          <span className="krum-option-group">{radios}</span>
          <span className="krum-option-group">
            <label>
              <input type="checkbox" checked={this.props.showMissing}
                onChange={this.showMissingChange} />
              &nbsp;Show missing episodes
            </label>
          </span>
        </div>
      </form>
    );
  }
});

module.exports = MediaFilters;
