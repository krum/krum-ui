/** @jsx React.DOM */

// libraries
var React = require('react');
var ramda = require('ramda');
var director = require('director');
var ContentActions = require('../actions/contentactions');

// components
var NavBar = require('./navbar');
var FileDropMixin = require('./filedropmixin');
var PlayControls = require('./playcontrols');
var UploadQueue = require('./uploadqueue');

// pages
var pages = require('../pages');

/**
 * The main App menus and # paths
 */
var APP_PATHS = [
  {path: "/latest",  text: "Latest",  page: pages.Latest},
  {path: "/browse",  text: "Browse",  page: pages.Browse},
  {path: "/unknown", text: "Unknown", page: pages.Unknown},
  {path: "/admin",   text: "Admin",   page: pages.Latest}
];

/**
 * The App class that we export.
 */
var Application = React.createClass({
  mixins: [FileDropMixin],

  propTypes: {
    core: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      uploads: [],
      router: this.buildRouter(APP_PATHS),
      path: undefined,
      page: undefined
    };
  },

  /**
   * Builds a director.Router out of a simple route->page spec.
   * state.page and state.route will be updated on route changes to
   * reflect when the page changes.
   *
   * If the page is routed to an unknown route it will be "redirected"
   * to the first route in the spec list.
   */
  buildRouter(specs) {
    var self=this;

    // build an object of {route -> setpage()}
    var routes = ramda.foldl((acc, val) => {
      var page = val.page;
      var path = val.path;

      acc[path] = () => {
        self.setState({
          page: page,
          path: path
        });
      };
      return acc;
    }, {}, specs);

    // just use the first
    var defaultRoute = specs[0].path;

    var router = director.Router(routes).configure({
      notfound: () => {
        router.setRoute(defaultRoute);
        // the router is async so we call handler inline to make sure
        // render() has valid page and path.
        routes[defaultRoute]();
      }
    });

    return router;
  },

  onFileDrop(e) {
    var dispatcher = this.props.core.dispatcher;

    // send of a "user wants to upload a file" action
    var files = e.dataTransfer.files;
    for (var i=0; i < files.length; i++) {
      dispatcher.handleViewAction(ContentActions.upload(files[i]));
    }

    // This tells FileDropMixin to stop the event bubble up.
    return false;
  },

  componentWillMount() {
    // only init the router now
    this.state.router.init();

    // send to default route if there isn't one already
    var route = this.state.router.getRoute();
    if (route[0] === "") {
      this.state.router.notfound();
    }
  },

  componentDidMount() {
    // hook in for file drops
    this.setDropTarget(document.body, this.onFileDrop);

    // subscribe for upload events
    var contentStore = this.props.core.stores.content;
    contentStore.on(contentStore.UPLOAD_PROGRESS, this._onUploadChange);
  },
  componentWillUnMount() {
    // hop out of upload subscriptions
    var contentStore = this.props.core.stores.content;
    contentStore.removeListener(contentStore.UPLOAD_PROGRESS, this._onUploadChange);
  },

  _onUploadChange() {
    var contentStore = this.props.core.stores.content;
    this.setState({uploads: contentStore.getUploads()});
  },

  render() {
    var Page = this.state.page;

    return (
      <div id="grid">
        <NavBar items={APP_PATHS} active={this.state.path} />
        <Page core={this.props.core} />
        <PlayControls core={this.props.core} />
        <UploadQueue core={this.props.core} uploads={this.state.uploads} />
      </div>
    );
  }
});

module.exports = Application;
