/** @jsx React.DOM */
require("babelify/polyfill");  // get's the runtime in there

var React = require('react');
var App = require('./components/app');
var Dispatcher = require('./services/dispatcher');
var stores = require('./stores');

/**
 * Main application state. Used to glue stores to dispatcher and
 * handed to pages so they can wire in functionality they need.
 */
var dispatcher = new Dispatcher();
var CoreObjects = {
  dispatcher: dispatcher,
  stores: {
    content: new stores.ContentStore(dispatcher),
    media: new stores.MediaStore(dispatcher),
    playbackSession: new stores.PlaybackSessionStore(dispatcher),
    playbackPlayer: new stores.PlaybackPlayerStore(dispatcher)
  }
};

// Render the app!
React.renderComponent(<App core={CoreObjects} />, document.body);
