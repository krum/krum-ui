/** @jsx React.DOM */

var React = require('react');
var ramda = require('ramda');
var StoreSliceMixin = require('../util/storeslicemixin');
var components = require('../components');

/**
 * Page that shows the latest shows/movies that can be watched.
 */
var BrowsePage = React.createClass({
  mixins: [StoreSliceMixin],

  propTypes: {
    core: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      mediaSlice: null,
      searchTerm: '',
      mediaTypes: [1, 3],
      showMissing: false
    };
  },

  componentDidMount() {
    this.rebuildMediaSlice();
  },

  rebuildMediaSlice(delta) {
    var actualState = ramda.mixin(this.state, delta);
    var APIFilters = {
      media_type: actualState.mediaTypes
    };
    if (actualState.showMissing === false) {
      // only get items whose content date is not empty (i.e. is present)
      APIFilters['!content_added_timestamp'] = '';
    }
    this.addSlice('mediaSlice', 'media', APIFilters, ['name']);
  },

  onFilterChange(delta) {
    console.log(delta);
    // update our state
    this.setState(delta);
    // update our resource Slice if that has changed
    if (delta.mediaTypes !== undefined || delta.showMissing !== undefined) {
      var apiDelta = ramda.pick(['mediaTypes', 'showMissing'], delta);
      this.rebuildMediaSlice(apiDelta);
    }
  },

  render() {
    var rows = [];
    if (this.state.mediaSlice !== null) {
      rows = this.state.mediaSlice.items;
    }

    // filter rows if we need
    var needle = ramda.toLowerCase(this.state.searchTerm);
    if (needle !== '') {
      rows = ramda.filter((r) => {
        var haystack = ramda.toLowerCase(r.name);
        return ramda.strIndexOf(needle, haystack) !== -1;
      }, rows);
    }

    return (
      <div id="page-content">
        <components.MediaFilters
          searchTerm={this.state.searchTerm}
          mediaTypes={this.state.mediaTypes}
          showMissing={this.state.showMissing}
          onChange={this.onFilterChange} />
        <components.MediaTable dispatcher={this.props.core.dispatcher} data={rows} />
      </div>
    );
  }
});

module.exports = BrowsePage;
