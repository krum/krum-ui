/** @jsx React.DOM */

var React = require('react');
var components = require('../components/index');
var ContentActions = require('../actions/contentactions');
var StoreSliceMixin = require('../util/storeslicemixin');

// How do we want to display the data
var COLDEF = {
  DELETE:   {
    attr: 'path',
    title: 'Delete',
    cellType: components.DeleteRowCell
  },
  FILEPATH: {
    attr: 'original_path',
    title: 'Filename'
  },
  TYPE:     {
    attr: 'mimetype',
    title: 'Type'
  },
  META:     {
    attr: 'metadata_id',
    title: 'Name',
    cellType: components.MetaAutocompleteCell
  }
};

var UnkownContentPage = React.createClass({
  mixins: [StoreSliceMixin],

  propTypes: {
    core: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      contentSlice: null
    };
  },

  /**
   * Set up the Content slice.
   */
  componentDidMount() {
    this.addSlice('contentSlice', 'content', {metadata_id:''});
  },

  /**
   * Alter the content according to the
   */
  _onTableChange(e) {
    var dispatcher = this.props.core.dispatcher;

    // send off the action to be actioned
    var path = e.krumData.rowData.path;
    switch (e.krumData.coldef) {
      case COLDEF.DELETE:
        dispatcher.handleViewAction(ContentActions.delete(path));
      break;
      case COLDEF.META:
        var id = e.krumData.choice.id;
        dispatcher.handleViewAction(ContentActions.link(path, id));
      break;
    }

    e.stopPropagation();
  },

  render() {
    var rows = [];
    if (this.state.contentSlice !== null) {
      rows = this.state.contentSlice.items;
    }
    return (
      <div id="page-content">
        <components.SelectTable
          coldefs={COLDEF}
          idcol={'path'}
          data={rows}
          onChange={this._onTableChange} />
      </div>
    );
  }
});

module.exports = UnkownContentPage;
