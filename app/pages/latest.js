/** @jsx React.DOM */

var React = require('react');
var StoreSliceMixin = require('../util/storeslicemixin');
var components = require('../components');

/**
 * Page that shows the latest shows/movies that can be watched.
 */
var LatestMediaPage = React.createClass({
  mixins: [StoreSliceMixin],

  propTypes: {
    core: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      mediaSlice: null
    };
  },

  componentWillMount() {
    this.addSlice('mediaSlice', 'media',
      {
        media_type: [1, 3],
        "!content_added_timestamp": ''
      }, ['-content_added_timestamp'], 20);
  },

  render() {
    var rows = [];
    if (this.state.mediaSlice !== null) {
      rows = this.state.mediaSlice.items;
    }

    return (
      <div id="page-content">
        <components.MediaTable dispatcher={this.props.core.dispatcher} data={rows} />
      </div>
    );
  }
});

module.exports = LatestMediaPage;
