/**
 * Defines the central dispatcher class.
 */

var BProm = require('bluebird');
var ramda = require('ramda');

function Dispatcher() {
  this._callbacks = [];    // List of event handlers
  this._promises = [];     // List of pending promises
}

Dispatcher.prototype = ramda.mixin(Dispatcher.prototype, {
  /**
   * Register a Store's callback so that it may be invoked by an action.
   * @param {function} callback The callback to be registered.
   * @return {number} The index of the callback within the _callbacks array.
   */
  register(callback) {
    this._callbacks.push(callback);
    return this._callbacks.length - 1; // index
  },

  /**
   * Handle an action that is user instigated.
   */
  handleViewAction(action) {
    this._dispatch({
      source: 'VIEW_ACTION',
      action: action
    });
  },

  /**
   * Send out the event to all listeners.
   * @param  {object} payload The data from the action.
   */
  _dispatch(payload) {
    // First create array of promises for callbacks to reference.
    var resolves = [];
    var rejects = [];
    this._promises = this._callbacks.map(function(_, i) {
      return new BProm(function(resolve, reject) {
        resolves[i] = resolve;
        rejects[i] = reject;
      });
    });
    // Dispatch to callbacks and resolve/reject promises.
    this._callbacks.forEach(function(callback, i) {
      // Callback can return an obj, to resolve, or a promise, to chain.
      // See waitFor() for why this might be useful.
      BProm.resolve(callback(payload)).then(function() {
        resolves[i](payload);
      }, function() {
        rejects[i](new Error('Dispatcher callback unsuccessful'));
      });
    });
    this._promises = [];
  },

  /**
   * Allows a store to wait for the registered callbacks of other stores
   * to get invoked before its own does.
   * This function is not used by this TodoMVC example application, but
   * it is very useful in a larger, more complex application.
   *
   * Example usage where StoreB waits for StoreA:
   *
   *   var StoreA = merge(EventEmitter.prototype, {
   *     // other methods omitted
   *
   *     dispatchIndex: Dispatcher.register(function(payload) {
   *       // switch statement with lots of cases
   *     })
   *   }
   *
   *   var StoreB = merge(EventEmitter.prototype, {
   *     // other methods omitted
   *
   *     dispatchIndex: Dispatcher.register(function(payload) {
   *       switch(payload.action.actionType) {
   *
   *         case MyConstants.FOO_ACTION:
   *           Dispatcher.waitFor([StoreA.dispatchIndex], function() {
   *             // Do stuff only after StoreA's callback returns.
   *           });
   *       }
   *     })
   *   }
   *
   * It should be noted that if StoreB waits for StoreA, and StoreA waits for
   * StoreB, a circular dependency will occur, but no error will be thrown.
   * A more robust Dispatcher would issue a warning in this scenario.
   */
  waitFor(/*array*/ promiseIndexes, /*function*/ callback) {
    var selectedPromises = promiseIndexes.map(index => {
      return this._promises[index];
    });
    return BProm.all(selectedPromises).then(callback);
  }
});

module.exports = Dispatcher;
