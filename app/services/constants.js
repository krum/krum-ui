
var ramda = require('ramda');

function keyMirror(d) {
  var keys = ramda.keys(d);
  var i=0;
  var newd = {};
  for (;i<keys.length; i++) {
    var k = keys[i];
    newd[k] = k;
  }
  return newd;
}

var Constants = keyMirror({
  CONTENT_UPLOAD: null,
  CONTENT_LINK: null,
  CONTENT_DELETE: null,

  PLAYBACK_CREATESESSION: null,
  PLAYBACK_TAKEOVERSESSION: null,
  PLAYBACK_DELETESESSION: null,
  PLAYBACK_APPENDPLAYLISTITEM: null,
  PLAYBACK_SKIPFIRSTITEM: null,
  PLAYBACK_PATCHSESSION: null
});

module.exports = Constants;
