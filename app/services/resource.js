/**
 * Defines helpers for API interactions.
 *
 * Note, this is tied to Krums URL patterns, as fleshed out in
 * util/apipaths.
 */

var ramda = require('ramda');
var BProm = require('bluebird');
var util = require('../util');

var MAX_QS_LEN = 1500; // for sanity!
var API_VERSION = 1;
var JSON_MIMETYPE_TYPE = 'application/json';
var JSON_MIMETYPE_FULL = 'application/json; charset=UTF-8';
var BINARY_MIMETYPE = 'application/octet-stream';

/**
 * Simple promisified wrapper around a single XHR request.
 *
 * @param {string} url - The URL to make the request to.
 * @param {string} method - The request METHOD.
 * @param {*} [body] - The data to upload (if any).
 * @param {string} [mimetype] - The mimetype to put in Content-Type
 *  header.
 * @param {object} [headers] - Additional headers to attach to the
 *  request.
 * @param {function} [progressHandler] - Will be set as the progress
 *  event handler if present.
 * Note: mimetype is only attached if body provided. If body is
 *       provided and mimetype is not, it defaults to
 *       "application/json; charset=UTF-8" for convenience.
 */
function http(url, method, body, mimetype, headers, progressHandler) {
  if (method === undefined) {
    method = 'GET';
  }

  return new BProm((resolve, reject) => {
    // make basic object
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('error', reject);
    xhr.addEventListener('load', resolve);
    // open it up, ready to start writing headers
    xhr.open(method, url);

    // do they want progress events?
    if (progressHandler !== undefined) {
      xhr.upload.onprogress = progressHandler;
    }

    // if body, set mimetype
    if (body !== undefined) {

      // default to JSON mimetype
      if (typeof(mimetype) !== "string") {
        mimetype = JSON_MIMETYPE_FULL;
      }

      xhr.setRequestHeader('Content-Type', mimetype);
    }

    // add headers
    var hkeys = ramda.keys(headers);
    var len = hkeys.length;
    for (var i = 0; i < len; i++) {
      var key = hkeys[i];
      var val = headers[key];
      xhr.setRequestHeader(key, val);
    }

    // send off the request
    xhr.send(body);
  });
}

/**
 * Builds up a query string value (without '?') from a set of
 * key-value pairs. Keys and values will be coerced to strings and URI
 * encoded.
 */
function buildQueryString(vals) {
  var parts = [];

  // make key=val bits
  var hkeys = ramda.keys(vals);
  var len = hkeys.length;
  for (var i = 0; i < len; i++) {
    var key = hkeys[i];
    var val = vals[key];
    if (Array.isArray(val)) {
      for (var j=0; j < val.length; j++) {
        parts.push(encodeURIComponent(key)+'='+encodeURIComponent(val[j]));
      }
    }
    else {
      parts.push(encodeURIComponent(key)+'='+encodeURIComponent(val));
    }
  }

  // return the all joined up with &'s
  var qs = parts.join('&');
  if (qs.length > MAX_QS_LEN) {
    console.trace('Warning: Query string longer than recommended: '+qs.length);
  }
  return qs;
}

/**
 * Takes either a resource name and optional id or a path and returns
 * a fully qualified URL.
 * @param {string} resourceName - The name of the resource type.
 * @param {string} path - The
 * @param {string|number} [id] -
 *
 * Note: One of resourceName or path must be provided, path has higher
 *       precedence. If provided, id means the URL will point at a
 *       specific resource instance instead of the collection.
 */
function urlFromPathOrResource(resourceName, path, id) {
  if (path === undefined) {
    return util.buildAPIURL(API_VERSION, resourceName, id);
  }
  else {
    return util.pathToURL(path);
  }
}

function parseIfJSON(xhr) {
  var ct = xhr.getResponseHeader('Content-Type');
  if (ct.startsWith(JSON_MIMETYPE_TYPE)) {
    return JSON.parse(xhr.responseText);
  }
  else {
    return xhr;
  }
}

var Resource = {
  JSON_MIMETYPE_TYPE: JSON_MIMETYPE_TYPE,
  JSON_MIMETYPE_FULL: JSON_MIMETYPE_FULL,
  BINARY_MIMETYPE: BINARY_MIMETYPE,

  // include util functions
  http: http,
  buildQueryString: buildQueryString,
  urlFromPathOrResource: urlFromPathOrResource,

  /**
   * Get a single or list of a particular resource.
   * @param {string} resourceName - The resource name.
   * @param {object} query - Key-value pairs for query string.
   * @param {string} path - The path to the resource.
   * @param {string|number} [id] - The specific resources' id to fetch.
   * @return - A promise that will resolve to the xhr.response. Or
   *           reject with the underlying xhr error event.
   *
   * Note: If path is provided it is used, otherwise resourceName and
   *       id are combined to build an API path. Provide id to get a
   *       specific resource, or leave out get the entire collection.
   */
  get: BProm.coroutine(function*(resourceName, query, path, id) {
    var url = urlFromPathOrResource(resourceName, path, id);

    // TODO: put in the query string build
    if (query !== undefined) {
      url += '?' + buildQueryString(query);
    }

    var e = yield http(url, 'GET');
    return parseIfJSON(e.target);
  }),
  /**
   * Creates a new resource and returns it's path or body if required.
   *
   * @param {string} resourceName - The name of the resource type.
   * @param {string} data - The XHR compatible object to send.
   * @param {bool} fetchBody - If true, the returned promise will
   *    resolve to the content of the newly created resource.
   * @return A promise that will resolve as the new content resources
   *    path or it's full content if fetchBody === true.
   */
  create: BProm.coroutine(function*(resourceName, data, fetchBody) {
    var url = urlFromPathOrResource(resourceName);

    var jd = JSON.stringify(data);
    var e = yield http(url, 'POST', jd);
    var xhr = e.target;

    // there was an error
    if (xhr.status < 200 || xhr.status > 299) {
      throw xhr;
    }

    // give 'em the data raw
    if (fetchBody !== true) {
      return xhr;
    }

    // try to get the resource body
    if (xhr.status === 200) {
      return parseIfJSON(xhr);
    }
    else if (xhr.status === 201) {
      // we have the path, get the body
      var path = xhr.getResponseHeader('Location');
      var e2 = yield http(urlFromPathOrResource(undefined, path), 'GET');
      var xhr2 = e2.target;

      if (xhr2.status === 200) {
        return parseIfJSON(xhr2);
      }
      else {
        throw xhr2;
      }
    }
  }),
  update: BProm.coroutine(function*(path, data, fetchBody) {
    var url = urlFromPathOrResource(undefined, path);

    var jd = JSON.stringify(data);
    var e = yield http(url, 'PATCH', jd);
    var xhr = e.target;

    // there was an error
    if (xhr.status < 200 || xhr.status > 299) {
      throw xhr;
    }

    // give 'em the data raw
    if (fetchBody !== true) {
      return xhr;
    }

    throw Error('not implemented yet');
  }),
  delete: BProm.coroutine(function*(resourceName, path, id) {
    var url = urlFromPathOrResource(resourceName, path, id);

    var e = yield http(url, 'DELETE');
    var xhr = e.target;

    // there was an error
    if (xhr.status < 200 || xhr.status > 299) {
      throw xhr;
    }

    return xhr;
  })
};

module.exports = Resource;
