/**
 * Defines a simple set of functions for the media uploader.
 */

var BProm = require('bluebird');
var ramda = require('ramda');
var resource = require('./resource');
var util = require('../util');

var PROGRESS_EVENT = 'progress';

/**
 * Provides a FIFO queue for file uploads. It will only upload a
 * single file at a time to the APIs.
 */
function UploadQueue(progressHandler) {
  this.currentUpload = null;
  this.pendingUploads = [];

  // set up our handler
  if (typeof(progressHandler) === 'function') {
    this.progressHandler = () => {
      progressHandler(this);
    };
  }
  else {
    this.progressHandler = () => {};
  }
}
UploadQueue.prototype = ramda.mixin(UploadQueue.Prototype, {
  isUploading() {
    return this.currentUpload !== null;
  },
  getUploads() {
    if (this.currentUpload !== null) {
      return ramda.append(this.currentUpload, this.pendingUploads);
    }
    else {
      return this.pendingUploads;
    }
  },
  /**
   * Add an upload to the FIFO queue
   */
  addUpload(file, destPath) {
    var newUpl = new FileUpload(file, util.pathToURL(destPath));
    this.pendingUploads = ramda.append(newUpl, this.pendingUploads);

    this._startUploads();
  },

  _startUploads() {
    // safety net
    if (this.isUploading() || this.pendingUploads.length < 1) {
      return;
    }

    // get the next one from the queue
    this.currentUpload = ramda.head(this.pendingUploads);
    this.pendingUploads = ramda.tail(this.pendingUploads);

    // kick it off
    this.currentUpload.upload(this.progressHandler)
      .catch(e => {
        // TODO: put in list of failed uploads or such, show reason...
        console.log(e);
      })
      .finally(() => {
        this.currentUpload = null;
        this.progressHandler();

        // go do the next one, if we have one.
        this._startUploads();
      });
  }
});

// Simple wrapper with some helper functions for a file object
function FileUpload(file, destURL) {
  this.file = file;
  this.destURL = destURL;
  this.transferStartDate = null;
  this.completedBytes = 0;
  this.totalBytes = Math.max(1, file.size);
}
FileUpload.prototype = ramda.mixin(FileUpload.Prototype, {
  /**
   * What's it's name?
   */
  getName() {
    return this.file.name;
  },

  /**
   * Returns 'waiting' if the upload hasn't started, else 'uploading'.
   */
  getStatus() {
    if (this.transferStartDate === null) {
      return 'waiting';
    }
    else {
      return 'uploading';
    }
  },
  /*
   * Returns the completed % as a real number.
   */
  getCompletedPercent() {
    return this.completedBytes/this.totalBytes;
  },
  /**
   * Returns the transfer rate in bytes/s.
   */
  getTFerRate() {
    if (this.transferStartDate === null) {
      return 0;
    }

    var elapsedSeconds = (new Date()-this.transferStartDate)/1000;
    return this.completedBytes/elapsedSeconds;
  },

  /**
   * Upload the entire file.
   *
   * The progress of the upload can be tracked by calling getStatus.
   *
   * @param {func} progressHandler - Called as the upload progresses.
   *  Single param is the upload itself.
   *
   * Returns
   *  A promise that will resolve once the upload of this file has
   *  completed.
   */
  upload: BProm.coroutine(function* (progressHandler) {
    // Set up starting info
    this.completed = 0;
    this.transferStartDate = new Date();
    var file = this.file;
    var slice = file.slice(0, file.size, file.type);

    var loadEvent = yield resource.http(
      this.destURL,
      'POST',
      slice,
      file.type,
      {},
      progressEv => {
        this.completedBytes = progressEv.loaded;

        if (progressHandler !== undefined) {
          progressHandler(this);
        }
      }
    );

    var xhrStatus = loadEvent.currentTarget.status;

    // if it wasn't a success
    if (xhrStatus < 200 || xhrStatus >= 300) {
      throw loadEvent.currentTarget.responseText;
    }
  })
});

module.exports = UploadQueue;
