
var ramda = require('ramda');
var BProm = require('bluebird');
var BaseStore = require('./basestore');
var constants = require('../services/constants');
var resource = require('../services/resource');
var util = require('../util');

var RESOURCE_NAME = 'playback/sessions';

// Event names
var EVENT_CHANGE = 'change';

/**
 * Playback Session store class.
 *
 * Allow for control of Playback Sessions, creation, changes, etc via
 * Actions.
 */
function PlaybackSessionStore(dispatcher) {
  // call the parent constructor
  BaseStore.call(this, dispatcher);

  // The session we're currently controlling
  this.controlSession = null;

  // load the session list, with onSessionsChange as callback
  this.allSessionSlice = this.createSlice(
    undefined, ['name'],
    undefined, undefined,
    this._onSessionsChange.bind(this)
  );

  // to merge changes
  this._inFlightPatchProm = null;
  this._waitingSessionPatch = {};
  this._waitingResolves = [];
  this._waitingRejects = [];

  // set up update polling
  var that=this;
  function doUpdate() {
    if (that.controlSession !== null) {
      that.updateAllSlices();
    }
    setTimeout(doUpdate, 1000);
  }
  doUpdate();
}
PlaybackSessionStore.prototype = ramda.mixin(BaseStore.prototype, {
  /**
   * Something has changed, control session, list of sessions, control
   * session details. So look and redraw if you need.
   */
  EVENT_CHANGE: EVENT_CHANGE,

  /**
   * Returns the resource name for BaseStore to build URLs/etc.
   */
  getResourceName() {
    return RESOURCE_NAME;
  },

  /**
   * Handlers all the PlaybackSession actions.
   */
  registerToDispatcher(dispatcher) {
    dispatcher.register(payload => {
      var action = payload.action;

      switch(action.actionType) {
        /* User has asked to create a playback session */
        case constants.PLAYBACK_CREATESESSION:
          return this.createNewSession(action.name, action.takeOver);
        break;

        /* User has asked to change a playback session */
        case constants.PLAYBACK_TAKEOVERSESSION:
          return this.setControlSession(action.path);
        break;

        /* User has asked to delete a playback session */
        case constants.PLAYBACK_DELETESESSION:
          if (this.controlSession && action.path === this.controlSession.path) {
            // don't send event
            this.setControlSession(null, false);
          }
          // delete will update the data set and send a CHANGE
          return this.delete(action.path);
        break;

        case constants.PLAYBACK_APPENDPLAYLISTITEM:
          return this.appendPlaylistItem(action.path);
        break;

        case constants.PLAYBACK_SKIPFIRSTITEM:
          return this.skipFirstPlaylistItem();
        break;

        case constants.PLAYBACK_PATCHSESSION:
          if (this.controlSession !== null) {
            return this.updateControlSession(action.delta);
          }
          else {
            return true;
          }
        break;

        default:
          return true;
        break;
      }
    });
  },

  /**
   * Get the Session we're currently controlling.
   */
  getControlSession() {
    return this.controlSession;
  },

  /**
   * Get the list of sessions available.
   */
  getAllSessions() {
    return this.allSessionSlice.items;
  },

  /**
   * Adds the requested patch to the queue.
   *
   * Each patch waits for the previous to complete. If there is
   * already an in flight patch, subsequent changes are merged into a
   * single patch to reduce requests.
   */
  updateControlSession(delta) {
    if (this.controlSession === null) {
      return;
    }

    // merge patch with other waiting changes
    this._waitingSessionPatch = ramda.mixin(this._waitingSessionPatch, delta);

    // add new prom to waiting list
    var prom = new Promise((resolve, reject) => {
      this._waitingResolves.push(resolve);
      this._waitingRejects.push(reject);
    });

    // try send patch
    this._sendSessionUpdate(this.controlSession.path);

    return prom;
  },

  /**
   * The tail-recursive patch send loop. Will terminate if the queue
   * is empty and be re-kicked off next time updateControlSession is
   * called.
   */
  _sendSessionUpdate(path) {
    // already one in flight, wait for the loop around.
    if (this._inFlightPatchProm !== null) {
      return;
    }

    if (this._waitingResolves.length === 0) {
      return;
    }

    // we're going to do it
    // grab the pending patch and waiting promises and clear queues
    var patch = this._waitingSessionPatch;
    this._waitingSessionPatch = {};
    var resolves = this._waitingResolves;
    this._waitingResolves = [];
    var rejects = this._waitingRejects;
    this._waitingRejects = [];

    // do the work wrapped to make sure we resolve/reject
    this._inFlightPatchProm = BProm.try(() => {
      // send off the request
      return this.update(path, patch);
    })
    .then(() => {
      // resolve each promise that was waiting on this send
      resolves.forEach((r) => { r(); });
    })
    .catch((e) => {
      // oh noes!, they all fail.
      rejects.forEach((r) => { r(e); });
      console.log("Error occured sending control session patch", e);
    })
    .finally(() => {
      // always clear this guy up so future request can go
      this._inFlightPatchProm = null;

      // kick off the next loop
      this._sendSessionUpdate(path);
    });

  },

  /**
   * creates the new session and optionally takes control of it.
   */
  createNewSession: BProm.coroutine(function*(name, takeOver) {
    // try to create the new session
    var newSessPath = yield this.create({name});

    if (takeOver === true) {
      this.setControlSession(newSessPath);
    }
  }),

  /**
   * Sets the session we're controlling, i.e. the one SESSIONCHANGE
   * actions will apply to.
   */
  setControlSession(path, emitChange) {
    // default to emitting event
    emitChange = emitChange !== false;

    var changed = false;
    if (path === null && this.controlSession !== null) {
      this.controlSession = null;
      changed = true;
    }
    else {
      // try to find one with matching path
      var newSession = ramda.find((session) => {
        return session.path === path;
      }, this.allSessionSlice.items);

      if (newSession !== undefined) {
        this.controlSession = newSession;
        changed = true;
      }
    }

    if (emitChange === true && changed === true) {
      this.emit(EVENT_CHANGE);
    }
  },

  _onSessionsChange() {
    // the slice it up-to-date, but we need to go get our new
    // ControlSession object
    if (this.controlSession === null) {
      this.emit(EVENT_CHANGE);
    }
    else {
      this.setControlSession(this.controlSession.path);
    }
  },

  /**
   * Adds the new item to the playlist
   */
  appendPlaylistItem: BProm.coroutine(function*(path) {
    if (this.controlSession === null) {
      return;
    }

    // get the details of the item
    var media_meta = yield resource.get(undefined, undefined, path);
    // if no content, complain
    if (media_meta.content.length === 0) {
      throw Error("Asked to add playlist item that lacks content.");
    }
    // get the content meta-data
    var content_path = media_meta.content[0];
    var content_meta = yield resource.get(undefined, undefined, content_path);

    // make new playlist
    var newItem = {
      id: media_meta.id,
      path: media_meta.path,
      name: media_meta.name,
      data_url: util.pathToURL(content_meta.data_url)
    };
    var newPlaylist = this.controlSession.playlist.concat([newItem]);

    // send off to the server (this updates the slices too)
    yield this.update(this.controlSession.path, {playlist: newPlaylist});
  }),

  /**
   * Drops the first item off the playlist.
   */
  skipFirstPlaylistItem() {
    if (this.controlSession === null) {
      return;
    }

    var playlist = this.controlSession.playlist;
    if (playlist.length < 1) {
      return;
    }

    // send off to the server (this updates the slices too)
    var newPlaylist = playlist.slice(1);
    return this.update(this.controlSession.path, {playlist: newPlaylist});
  }
});

module.exports = PlaybackSessionStore;
