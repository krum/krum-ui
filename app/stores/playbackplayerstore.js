
var ramda = require('ramda');
var BaseStore = require('./basestore');

var RESOURCE_NAME = 'playback/players';

/**
 * Playback Remote Player store.
 *
 * Creates slices (live updating views I guess) of the Remote Player
 * collection.
 */
function PlaybackPlayerStore(dispatcher) {
  // call the parent constructor
  BaseStore.call(this, dispatcher);
}
PlaybackPlayerStore.prototype = ramda.mixin(BaseStore.prototype, {
  /**
   * Returns the resource name for BaseStore to build URLs/etc.
   */
  getResourceName() {
    return RESOURCE_NAME;
  },
});

module.exports = PlaybackPlayerStore;
