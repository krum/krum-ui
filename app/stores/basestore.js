
var ramda = require('ramda');
var BProm = require('bluebird');
var EventEmitter = require('events').EventEmitter;
var resource = require('../services/resource');
var util = require('../util');

function ResourceSlice(resource_name, filters, onChange) {
  this.disabled = false;
  this.resource_name = resource_name;
  this.filters = filters;
  this.onChange = onChange;
  this.items = [];
  this.forEach = this.items.forEach;
}
ResourceSlice.prototype = ramda.mixin(ResourceSlice.prototype, {
  /**
   * Clears the callback and ensures it won't update.
   */
  disable() {
    this.onChange = undefined;
    this.disabled = true;
  },
  /**
   * Updates this slice, returning a Promise that will yield the slice
   * once the update has come back and been processed.
   */
  update: BProm.coroutine(function*() {
    if (this.disabled === true) {
      return this;
    }

    var items = yield resource.get(this.resource_name, this.filters);

    if (items.list === undefined) {
      throw Error(util.formatStr(
                  'ResourceSlice({}).Update: Returned JSON object is missing "list" attribute.',
                  this.resource_name
                  ));
    }

    this._replaceItems(items.list);

    return this;
  }),
  _sendUpdate() {
    if (typeof(this.onChange) === 'function') {
      this.onChange(this);
    }
  },
  _replaceItems(newItems) {
    this.items = newItems;
    this.forEach = this.items.forEach;

    this._sendUpdate();
  }
});

/**
 * BaseStore class provides all the common Store methods, createSlice,
 * destroySlice, delete, etc.
 *
 * Type specific methods, e.g. for handling UI actions, should be
 * defined in subclasses.
 */
function BaseStore(dispatcher) {
  this._activeSlices = [];

  this.registerToDispatcher(dispatcher);
}
BaseStore.prototype = ramda.mixin(EventEmitter.prototype, {
  /**
   * Returns the name of the resource for API path construction/etc.
   *
   * Must be implemented by subclasses.
   */
  getResourceName() {
    throw Error('getResourceName() Not implemented in subclass');
  },

  /**
   * This is called on construction and allows subclasses register to
   * the dispatcher for server/UI/etc action handling.
   */
  registerToDispatcher(dispatcher) {
  },

  /**
   * Create an auto-updating filtered, view of the resource collection
   * (aka a "slice").
   *
   * Idea is that you can request a particular view that is filtered,
   * ordered and paginated for the particular UI component and the
   * the Store will tell you when it has updated (if you provide an
   * onChange event handler).
   *
   * @param filters {object} - Filter down to the specific objects you
   *  want.
   * @param ordering {array} - List of attribute names to sort by.
   *  These are used to sort the data in decreasing precedence. Put a
   *  minus char, i.e. "-", at the front of the attribute name to sort
   *  that attribute descending, otherwise it will sort ascending.
   * @param onChange {func} - Called when the data underlying this
   *  slice changes. onChange is called with 1 param, the new slice.
   */
  createSlice(filters, ordering, limit, offset, onChange) {
    // set up ordering, offset and limit filters
    var flts = {};
    if (ordering !== undefined) {
      flts.ord = ordering.join('');
    }
    if (limit !== undefined) {
      flts.limit = limit;
    }
    if (offset !== undefined) {
      flts.offset = offset;
    }
    flts = ramda.mixin(flts, filters);

    // create new slice from filters
    var s = new ResourceSlice(this.getResourceName(), flts, onChange);

    // add to list so it gets updated
    this._activeSlices.push(s);

    // make request for initial data
    s.update()
    .catch((e) => {
      console.log("Error occured getting slice!", e);
    });

    return s;
  },

  /**
   * Delete our reference to the slice and stop futher onChange calls.
   */
  destroySlice(slice) {
    var slices = this._activeSlices;
    // find it
    var idx = ramda.indexOf(slice, slices);
    if (idx === -1) {
      return;
    }

    // kill it
    slice.disable();
    // forget it
    this._activeSlices = ramda.remove(idx, 1, slices);
  },

  /**
   * Will perform an API hit to update each slice.
   *
   * Returns a promise that will complete when the update(s) is(are)
   * complete.
   */
  updateAllSlices: BProm.coroutine(function*() {
    var slices = this._activeSlices;
    for (var i=0; i < slices.length; i++) {
      yield slices[i].update();
    }
  }),

  /**
   * Will request the APIs create one or more new resources with the
   * given attributes.
   *
   * @param data {obj or Array} - Use a single object to create a
   *  single new instance of the resource or an array to create
   *  multiple.
   * @return {Promise} - A new promise that will complete once the
   *  create request is complete and the slices have all been updated.
   *  This will throw an error if the request got a non-200 response
   *  or encountered an error.
   */
  create: BProm.coroutine(function*(data) {
    // create it
    var createXHR = yield resource.create(this.getResourceName(), data);

    var newResourcePath = createXHR.getResponseHeader('Location');
    createXHR = null;

    // now update our slices
    yield this.updateAllSlices();

    return newResourcePath;
  }),

  /**
   * Request the API PATCH the resource with the given delta object.
   *
   * The PATCH process is a simple attribute replacement on the
   * server.
   *
   * @param path {string} - The API path to this resource.
   * @param delta {object} - The set of attributes to be replaced on
   *  the server.
   * @return {promise} - Will resolve once the update is complete and
   *  all slices have been updated too.
   */
  update: BProm.coroutine(function*(path, delta) {
    // update it
    yield resource.update(path, delta);

    // now update our slices
    yield this.updateAllSlices();
  }),

  /**
   * Will request the APIs delete one or more resources based on path.
   */
  delete: BProm.coroutine(function*(paths) {
    if (typeof(paths) === 'string') {
      paths = [paths];
    }
    else if (!Array.isArray(paths)) {
      throw Error('paths param must be string or array of strings.');
    }

    paths = ramda.values(paths);
    for (var i=0; i < paths.length; i++) {
      // explicit that resource should take the path, not build one
      yield resource.delete(undefined, paths[i]);
    }

    yield this.updateAllSlices();
  }),
});

module.exports = BaseStore;
