
var ramda = require('ramda');
var BProm = require('bluebird');
var BaseStore = require('./basestore');
var constants = require('../services/constants');
var resource = require('../services/resource');
var UploadQueue = require('../services/uploader');

var RESOURCE_NAME = 'content';
var UPLOAD_PROGRESS = 'uploadprogress';

/**
 * Content store class.
 *
 * Create slices (live updating views I guess) of the content
 * collection.
 */
function ContentStore(dispatcher) {
  // call the parent constructor
  BaseStore.call(this, dispatcher);

  // we need an upload queue
  this._uploader = new UploadQueue(() => {this._onUploadProgress();});
}
ContentStore.prototype = ramda.mixin(BaseStore.prototype, {
  UPLOAD_PROGRESS: UPLOAD_PROGRESS,

  /**
   * returns the resource name for BaseStore to build URLs/etc.
   */
  getResourceName() {
    return RESOURCE_NAME;
  },

  /**
   * Content Store action handling.
   */
  registerToDispatcher(dispatcher) {
    dispatcher.register(payload => {
      var action = payload.action;

      switch(action.actionType) {
        /* User has asked to upload some content */
        case constants.CONTENT_UPLOAD:
          return this.upload(action.file);

        /* user has asked to delete some content */
        case constants.CONTENT_DELETE:
          return this.delete(action.path);

        case constants.CONTENT_LINK:
          return this.link_content(action.path, action.metadata_id);

        default:
          return true;
      }
    });
  },

  /**
   * Tells krum what this content is (i.e. Star Wars vs Seinfeld S01E01, etc).
   */
  link_content: BProm.coroutine(function*(path, metadata_id) {
    var change = {
      metadata_id: metadata_id
    };
    yield resource.update(path, change, false);

    return this.updateAllSlices();
  }),

  /**
   * Called each time there is a some change in upload progress.
   */
  _onUploadProgress() {
    this.emit(UPLOAD_PROGRESS);
  },

  /**
   * Create the content metadata and upload the file.
   *
   * @return {Promise} That resolves to the
   */
  upload: BProm.coroutine(function*(file) {
    // create the new content metadata object
    var content = yield resource.create(RESOURCE_NAME, {
      mimetype: file.type,
      original_path: file.name
    }, true);

    // kick off the upload
    this._uploader.addUpload(file, content.data_url);

    yield this.updateAllSlices();

    return content;
  }),

  getUploads() {
    return this._uploader.getUploads();
  }
});

module.exports = ContentStore;
