
var ramda = require('ramda');
var BProm = require('bluebird');
var BaseStore = require('./basestore');
var constants = require('../services/constants');
var resource = require('../services/resource');

var RESOURCE_NAME = 'media';

/**
 * Media store class.
 *
 * Creates slices (live updating views I guess) of the Media
 * collection.
 * Handles all Media related actions (User and server originated).
 */
function MediaStore(dispatcher) {
  // call the parent constructor
  BaseStore.call(this, dispatcher);
}
MediaStore.prototype = ramda.mixin(BaseStore.prototype, {
  /**
   * Returns the resource name for BaseStore to build URLs/etc.
   */
  getResourceName() {
    return RESOURCE_NAME;
  },
});

module.exports = MediaStore;
