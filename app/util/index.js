/**
 * This module bundles up all the utility functions for convenience.
 */

var ramda = require('ramda');

var funcs = {};
funcs = ramda.mixin(funcs, require('./apipaths'));
funcs = ramda.mixin(funcs, require('./compare'));
funcs = ramda.mixin(funcs, require('./string'));

funcs.StoreSliceMixin = require('./storeslicemixin');

module.exports = funcs;
