/**
 * Module that easily allows building up paths for Krum API requests.
 *
 * The Krum APIs follow a very simple structure.
 *  api/{version}/{resource}[/{id}]
 * where:
 *  version is the version of the API in question
 *  resource is the name of the resource
 *  id is an optional parameter to make the request target a single
 *    instance of that particular resource.
 */

var str = require('./string');

/**
 * Join two paths or path segments.
 * @param {string} a - Can end with / or not.
 * @param {string} b - Can start with / or not.
 * @return a joined with b by 1 and only 1 '/' char.
 */
function pathJoin(a, b) {
  if (a[a.length-1] !== '/') {
    a += '/';
  }
  if (b[0] === '/') {
    b = b.slice(1);
  }

  return a+b;
}

var APIPaths = {
  // include util functions
  pathJoin: pathJoin,

  pathToURL(path) {
    return pathJoin(UICONFIG.API_URL_BASE, path);
  },
  buildAPIURL(apiversion, resource, id) {
    return APIPaths.pathToURL(APIPaths.buildAPIPath(apiversion, resource, id));
  },
  buildAPIPath(apiversion, resource, id) {
    var path = str.formatStr('/api/{}/{}', apiversion, resource);

    if (id !== undefined) {
      path += '/'+id;
    }

    return path;
  }
};

module.exports = APIPaths;
