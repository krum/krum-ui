var StringUtils = {
  /**
   * A simple string formatter.
   *
   * Args:
   *    formatStr string: The format string.
   *    ...: Remaining arguments are pushed in to an array to allow
   *         insertion according to formatStr.
   *
   * {} or {N}, where N is an integer, can be used in formatStr to
   * indicate where arguments should be merged in to the string.
   * If N is supplied, that number is used to select the argument.
   * If N is not supplied, the ordinal of the {} itself will be
   * used to select which argument is interpolated.
   *
   * E.g:
   *    formatStr("Automatic: {} of {}", 12, "fish")
   *      -> "Automatic: 12 of fish"
   *    formatStr("Manual: {0}, {1}, {1}", "bob", 1, 2)
   *      -> "Manual: bob, 1, 1"
   *    formatStr("Manual: {0}, {1}, {1}", "bob", 1, 2)
   *      -> "Manual: bob, 1, 1"
   */
  formatStr(str) {
    var params = Array.prototype.slice.call(arguments, 1);
    var base = 0;
    var parts = [];
    var re = /\{([0-9]*)\}/gi;
    var r = null;
    var i = 0;
    while ((r = re.exec(str)) !== null) {
      // grab pre-segment of string
      parts.push(str.slice(base, re.lastIndex-r[0].length));
      base = re.lastIndex;
      // get arg to slice in
      var index = (r[1] === '' ? i : parseInt(r[1], 10));
      var param = params[index];
      if (param === undefined) {
        throw {name: 'indexError', message: 'Argment index outside range, text '+r[0]+' index '+r[1]};
      }
      parts.push(param);
      i += 1;
    }
    if (base !== str.length) {
      parts.push(str.slice(base, str.length));
    }
    return parts.join('');
  },

  secondsToHMS(seconds) {
    // make sure it's a number
    seconds = parseInt(seconds, 10);

    // break into hours, minutes, seconds
    var parts = [];
    if (seconds > 3600) {
      parts.push(Math.floor(seconds/3600));
    }
    if (seconds > 60) {
      parts.push(Math.floor((seconds%3600)/60));
    }
    parts.push(Math.floor(seconds%60));

    // turn into string
    var val = '';
    for (var i=0; i < parts.length; i++) {
      var p = ''+parts[i];

      // where the leftmost number isn't 0 padded
      if (i === 0) {
        val += parts[i];
      }
      // but others are and have a : in front of them
      else {
        val += ':' + (p.length === 1 ? '0'+p : p);
      }
    }

    return val;
  },

  truncateStr(s, len) {
    if (s.length <= len) {
      return s;
    }
    else {
      return s.slice(0, len-3) + '...';
    }
  }
};

module.exports = StringUtils;
