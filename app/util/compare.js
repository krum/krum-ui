
var ramda = require('ramda');

/**
 * Order independent, shallow Array comparison.
 */
function eqArrayShallowNoOrder(a, b) {
  if (!Array.isArray(a) || !Array.isArray(b)) {
    return false;
  }
  if (a === b) {
    return true;
  }
  if (a.length !== b.length) {
    return false;
  }
  // check b contains each of a
  return ramda.reduce((isSame, x) => {
    return isSame && ramda.indexOf(x, b) !== -1;
  }, true, a);
}

exports.eqArrayShallowNoOrder = eqArrayShallowNoOrder;
