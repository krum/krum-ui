
var ramda = require('ramda');

/**
 * Provides simple lifecycle management for watching stores.
 */
var StoreSliceMixin = {
  componentWillMount() {
    this._slices = {};
  },

  _onSliceChange: ramda.curry(function(name, store, newSlice) {
    this._slices[name] = [store, newSlice];
    if (this.isMounted()) {
      var newstate = {};
      newstate[name] = newSlice;
      this.setState(newstate);
    }
    else {
      console.log("I'm not mounted.");
    }
  }),

  /**
   * Creates a new named slice.
   *
   * this.setState will be called when the data for the slice
   * initially arrives and whenever it changes.
   *
   * @param name {string} - The name to give this slice. This is used
   *  as the key in the setState call.
   * @param storeName {string} - The name of the store to pull it from.
   * @param filters {string->string} - utils.http style filter map.
   * @param ordering {string} - utils.http style ordering.
   * @param limit {int} - utils.http style limit.
   * @param offset {int} - utils.http style offset.
   * @returns {stores.ResourceSlice} - The resource slice that will be
   *  updated as changes occur to the store/API/etc.
   */
  addSlice(name, storeName, filters, ordering, limit, offset) {
    // clean up if that name already exists (to allow filter/etc changes)
    this.deleteSlice(name);

    var store = this.props.core.stores[storeName];

    // _onSliceChange is curried, so partially apply it.
    var cb = this._onSliceChange(name, store).bind(this);
    var slice = store.createSlice(filters, ordering, limit, offset, cb);

    // save slice
    this._slices[name] = [store, slice];
  },
  /**
   * Delete an added slice. This will also clear from this.state.
   */
  deleteSlice(name) {
    // skip if it doesn't exist
    var obj = this._slices[name];
    if (obj === undefined) {
      return;
    }
    // clear it out (from us and this.state)
    delete this._slices[name];

    // now clear it up at the store
    var store = obj[0];
    var slice = obj[1];
    store.destroySlice(slice);
  },
  componentWillUnmount() {
    ramda.keys(this._slices).forEach((name) => {
      this.deleteSlice(name);
    }, this);
  },
};

module.exports = StoreSliceMixin;
