/**
 * Defines the UI Actions for Content objects.
 */
var Constants = require('../services/constants');

var PlaybackSessionActions = {
  /**
   * @param  {string} data
   */
  create(name, takeOver) {
    return {
      actionType: Constants.PLAYBACK_CREATESESSION,
      name: name,
      takeOver: takeOver === true
    };
  },
  /**
   * Make this the session we're controlling
   */
  takeOver(path) {
    return {
      actionType: Constants.PLAYBACK_TAKEOVERSESSION,
      path: path
    };
  },
  /**
   * Delete the session, and set the current session to null if it's
   * also the one we're controlling.
   */
  delete(path) {
    return {
      actionType: Constants.PLAYBACK_DELETESESSION,
      path: path
    };
  },
  /**
   * Add a new piece of media to the playlist.
   */
  appendPlaylistItem(path) {
    return {
      actionType: Constants.PLAYBACK_APPENDPLAYLISTITEM,
      path: path
    };
  },
  /**
   * Skip (i.e. drop) the first playlist item.
   */
  skipFirstPlaylistItem() {
    return {
      actionType: Constants.PLAYBACK_SKIPFIRSTITEM
    };
  },
  /**
   * A generic action representing a change to the control Session.
   * Applied as a set of attribute updates.
   */
  patchSession(delta) {
    return {
      actionType: Constants.PLAYBACK_PATCHSESSION,
      delta: delta
    };
  }
};

module.exports = PlaybackSessionActions;
