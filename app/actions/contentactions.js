/**
 * Defines the UI Actions for Content objects.
 */
var Constants = require('../services/constants');

var ContentActions = {
  /**
   * @param  {string} data
   */
  upload(file) {
    return {
      actionType: Constants.CONTENT_UPLOAD,
      file: file
    };
  },
  delete(path) {
    return {
      actionType: Constants.CONTENT_DELETE,
      path: path
    };
  },
  link(path, metadata_id) {
    return {
      actionType: Constants.CONTENT_LINK,
      path: path,
      metadata_id: metadata_id
    };
  }
};

module.exports = ContentActions;
