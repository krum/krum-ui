"use strict";

var gulp = require('gulp');
var glob = require('glob');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require("babelify");
var less = require('gulp-less');
var express = require('express');
var expressReload = require('connect-livereload');
var tinyReload = require('tiny-lr');
var fs = require('fs');
var path = require('path');

// read external package list from package.json
var EXTERNAL_LIBS = (function() {
  var pkgPath = path.join(__dirname, 'package.json');
  var pjson = JSON.parse(fs.readFileSync(pkgPath, 'utf8'));
  return Object.keys(pjson.dependencies);
})();

var WEB_PORT = 9000;
var RELOAD_PORT = 35729;

// Pathing info
var DISTDIR = 'dist';
var FA_FONTS = 'node_modules/font-awesome/fonts/*';

/**
 * Key is task name (for convenience)
 *  val is [src files, dest dir, dest file name]
 *    dest file name is only needed for JS browserify builds.
 */
var PATHS = {
  libcode:  [null, DISTDIR, 'libs.js'],
  appcode:  [['./app/main.jsx'], DISTDIR, 'app.js'],
  testcode: [['./tests/**/*.spec.js'], DISTDIR, 'tests.js'],
  copy:     [['app/*.html', FA_FONTS], DISTDIR],
  style:    [['app/main.less'], DISTDIR],
  config:   [['app/uiconfig.js'], DISTDIR]
};
/**
 * key is what task(s) to run, val is which files to watch
 */
var WATCHES = {
  libcode: ['package.json'],
  appcode: ['app/**/*.jsx', 'app/**/*.js'],
  testcode:['app/**/*.jsx', 'app/**/*.js', './tests/**/*.spec.js'],
  copy:    ['app/*.html', FA_FONTS],
  style:   ['app/**/*.less'],
  config:  ['app/uiconfig.js']
};

/**
 * Simple error printing function. Will make more complex if needed.
 */
function printError() {
  console.log.apply(null, arguments);
  console.log("I AM ERROR");
}

/**
 * 1. Takes entry points
 * 2. runs through browserify with reactify transform so that jsx
 *    files work too.
 * 3. pipes through traceur for ES6->ES5 compilation.
 * 4. Writes out as outfile to outdir.
 *
 * Note: If debug===True, will provide sourcemaps.
 */
function bundleES6JSX(entryPoints, outfile, outdir, debug) {

  // find the actual files
  var files = [];
  entryPoints.forEach(function(i, elem) {
    files = files.concat(glob.sync(entryPoints[elem]));
  });

  (function bundleOrNothing() {
    try {
      var b = browserify({
        entries: files,
        debug: debug,
        extensions: ['.jsx'],
        detectGlobals: false        // I don't use, off -> faster builds
      });

      // tell browserify which libs are external
      EXTERNAL_LIBS.forEach(function(lib) {
        b.external(lib);
        return true;
      });
      // we want the runtime too
      b.external("babelify/polyfill");

      return b
        .transform(babelify.configure({
          optional: ["reactCompat"]
        }))
        .bundle()
        .on('error', printError)
        .pipe(source(outfile));
    }
    catch (a) {
      return gulp.src(['./noop.js']);
    }
  })()
    // This collects the bundled output and renames it so gulp can
    // write it out.
    .pipe(gulp.dest(outdir));
}

/**
 * main.less -> build single .css
 */
gulp.task('style', function() {
  return gulp.src(PATHS.style[0])
    .pipe(less())
    .on('error', printError)
    .pipe(gulp.dest(PATHS.style[1]));
});

/**
 * This builds up the slow moving libraries. We do this to speed up
 * the overall packaging process for dev.
 *
 * It also allows us to ensure that the only included external libs
 * are those that are in package.json -> depdendencies. Remove some
 * dumb checkin issues :).
 */
gulp.task('libcode', function() {
  var cfg = PATHS.libcode;
  return browserify({
    entries: ['./noop.js'],
    debug: false,
    require: EXTERNAL_LIBS,
  })
    .require("babelify/polyfill")
    .bundle()
    .on('error', printError)
    .pipe(source(cfg[2]))
    .pipe(gulp.dest(cfg[1]));
});

/**
 * This builds up the app specific code (i.e. stuff in this repo).
 *
 * External libs (react, etc) are bunded up in the 'libcode' task.
 */
gulp.task('appcode', function() {
  var cfg = PATHS.appcode;
  return bundleES6JSX(cfg[0], cfg[2], cfg[1], true);
});

/**
 * This builds all the test specs into a single file for running.
 *
 * External libs (react, etc) are bunded up in the 'libcode' task.
 */
gulp.task('testcode', function() {
  var cfg = PATHS.testcode;
  return bundleES6JSX(cfg[0], cfg[2], cfg[1], true);
});

/**
 * Copy over index.html and fonts
 */
gulp.task('copy', function() {
  var cfg = PATHS.copy;
  return gulp.src(cfg[0])
    .pipe(gulp.dest(cfg[1]));
});

/**
 * Copy over the dev-only API config file.
 */
gulp.task('config', function() {
  var cfg = PATHS.config;
  return gulp.src(cfg[0])
    .pipe(gulp.dest(cfg[1]));
});

/**
 * Set up LiveReloading static file server
 */
function serveAndWatchFiles() {
  console.log('++Starting webserver');
  var app = express();
  app.use(expressReload());
  app.use(express.static(DISTDIR));
  app.listen(WEB_PORT);

  // start live reload server
  var lr = tinyReload();
  lr.listen(RELOAD_PORT);

  // start up the karma server
//  var k = startKarma();

  /**
   * Tell LiveReload to reload!
   */
  function sendReload(e) {
    console.log('Sending reload', e);
    lr.changed({
      body: {
        files: [path.relative(DISTDIR, e.path)]
      }
    });
  }

  console.log('++Starting watches');
  // run the per process watches
  for (var task in WATCHES) {
    gulp.watch(WATCHES[task], [task]);
  }
  // If any of the build files have changed, send a reload to the browser
  gulp.watch(DISTDIR+'/*').on('change', sendReload);
}

gulp.task('default', [
    'style', 'libcode', 'appcode', 'copy', 'config'
  ], serveAndWatchFiles);
