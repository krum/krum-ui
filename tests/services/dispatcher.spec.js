/** @jsx React.DOM */

var Dispatcher = require('../../app/services/dispatcher');

describe('Dispatcher', () => {
  var dispatcher = null;
  beforeEach(() => {
    dispatcher = new Dispatcher();
  });

  it('should hand back ids', () => {
    var ident = dispatcher.register(() => {});
    expect(ident).to.be.a('number');
  });

  it('should call funcs', () => {
    var cb = sinon.spy();

    var dataSent = {'cat': 1, 'apple': 'card', 2: [1,2,4]};
    dispatcher.register(cb);
    dispatcher._dispatch(dataSent);
    expect(cb).to.have.been.calledWith(dataSent);
  });

  it('should wrap viewActions', () => {
    var cb = sinon.spy();

    var dataSent = {'cat': 1, 'apple': 'card', 2: [1,2,4]};
    var viewAction = {
      source: 'VIEW_ACTION',
      action: dataSent
    };
    dispatcher.register(cb);
    dispatcher.handleViewAction(dataSent);
    expect(cb).to.have.been.calledWith(viewAction);
  });
});
