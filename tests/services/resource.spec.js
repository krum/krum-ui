
var resource = require('../../app/services/resource');

describe('Resource', () => {
  // set up the API config
  var urlBase = 'http://base.base';
  before(() => {
    window.UICONFIG = {API_URL_BASE: urlBase};
  });

  describe('buildQueryString', () => {
    it('should allow empty', () => {
      expect(resource.buildQueryString({})).to.equal('');
    });
    it('should work with 1', () => {
      expect(resource.buildQueryString({a:'forge'})).to.equal('a=forge');
    });
    it('should encode space and %', () => {
      expect(resource.buildQueryString({term:'my 100%'})).to.equal('term=my%20100%25');
    });
    it('should encode keys too', () => {
      expect(resource.buildQueryString({'te rm':'my 100%'})).to.equal('te%20rm=my%20100%25');
    });
    it('should work with many', () => {
      var data = {};
      var val = '';
      for (var i=0; i<100; i++) {
        var c = String.fromCharCode(i);
        data['a'+i] = c;
        val += '&a'+i+'='+encodeURIComponent(c);
      }
      val = val.slice(1);
      expect(resource.buildQueryString(data)).to.equal(val);
    });
    it('should work with arrays', () => {
      var data = {
        media_type: [1,3]
      };
      expect(resource.buildQueryString(data)).to.equal('media_type=1&media_type=3');
    });
  });

  describe('urlFromPathOrResource', () => {
    it('should build collection urls', () => {
      expect(resource.urlFromPathOrResource('content'))
        .to.equal(urlBase+'/api/1/content');
    });
    it('should build instance urls', () => {
      expect(resource.urlFromPathOrResource('content', undefined, 2424))
        .to.equal(urlBase+'/api/1/content/2424');
    });
    it('should use path if present', () => {
      expect(resource.urlFromPathOrResource('content', 'testoing', 2424))
        .to.equal(urlBase+'/testoing');
    });
  });
});
