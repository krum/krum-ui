
var BProm = require('bluebird');
var resource = require('../../app/services/resource');
var Dispatcher = require('../../app/services/dispatcher');
var ContentStore = require('../../app/stores/contentstore');

describe('ContentStore', () => {
  var content = null;
  beforeEach(() => {
    content = new ContentStore(new Dispatcher());
  });

  describe('slices', () => {
    // replace resource.get
    var getStub = null;
    beforeEach(() => {
      getStub = sinon.stub(resource, 'get');
    });
    // restore resource.get
    afterEach(() => {
      getStub.restore();
      getStub = undefined;
    });

    it('should provide initial data', BProm.coroutine(function*() {
      // set up first fake get response
      getStub.onFirstCall().returns(
        new BProm((resolve) => {
          resolve({list: []});
        })
      );

      var resolve, reject;
      var prom = new BProm((res, rej) => {
        resolve = res;
        reject = rej;
      });
      function onChange() {
        resolve();
      }

      // generate the GET request (which we stubbed out)
      var filters = {metadata_id: null};
      var slice = content.createSlice(filters, undefined, undefined, undefined, onChange);
      // wait till onChange gets called...
      yield prom;

      // check it was called correctly
      expect(getStub).to.have.been.calledWith('content', filters);
      expect(slice.items).to.have.length(0);

      // and we should now have the response
      content.destroySlice(slice);
    }));
  });

  describe('', () => {
  });
});
