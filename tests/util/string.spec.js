
var util = require('../../app/util');

describe('Util', () => {
  describe('secondsToHMS', () => {
    it('works for < minute', () => {
      expect(util.secondsToHMS(2)).to.equal('2');
      expect(util.secondsToHMS(59.77)).to.equal('59');
      expect(util.secondsToHMS(0)).to.equal('0');
    });
    it('works for < hour', () => {
      expect(util.secondsToHMS(72)).to.equal('1:12');
      expect(util.secondsToHMS(3599.8)).to.equal('59:59');
    });
    it('works for > hour', () => {
      expect(util.secondsToHMS(12072)).to.equal('3:21:12');
    });
    it('converts strings to numbers', () => {
      expect(util.secondsToHMS('12072')).to.equal('3:21:12');
    });
    it('converts strings to numbers', () => {
      expect(util.secondsToHMS('12072')).to.equal('3:21:12');
    });
  });
});
