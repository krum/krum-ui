/** @jsx React.DOM */

var util = require('../../app/util');

describe('Util', () => {
  describe('pathJoin', () => {
    it('works with segments', () => {
      expect(util.pathJoin('a', 'b')).to.equal('a/b');
    });
    it('works with a path and segment', () => {
      expect(util.pathJoin('/a/1', 'b')).to.equal('/a/1/b');
    });
    it('works with trailing slash', () => {
      expect(util.pathJoin('/a/1/', 'b')).to.equal('/a/1/b');
    });
    it('works with leading slash', () => {
      expect(util.pathJoin('/a/1', '/b')).to.equal('/a/1/b');
    });
    it('works with both slashes', () => {
      expect(util.pathJoin('/a/1/', '/b')).to.equal('/a/1/b');
    });
    it('works with baseURL + path', () => {
      var baseURL = 'http://b:a@somehost:9080/a/1/';
      expect(util.pathJoin(baseURL, '/b/1/s')).to.equal(baseURL+'b/1/s');
    });
  });

  describe('buildAPIPath', () => {
    it('Can build /api/1/content', () => {
      expect(util.buildAPIPath(1, 'content')).to.equal('/api/1/content');
    });
  });

  describe('buildAPIURL', () => {
    // Set up config
    before(() => {
      window.UICONFIG = {
        API_URL_BASE: 'http://localhost'
      };
    });

    it('Can build http://localhost/api/1/content', () => {
      expect(util.buildAPIURL(1, 'content')).to.equal(
             'http://localhost/api/1/content');
    });
  });
});
