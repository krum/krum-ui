/** @jsx React.DOM */

var ramda = require('ramda');
var util = require('../../app/util');

describe('Util', () => {
  describe('eqArrayShallowNoOrder', () => {
    // name is soooo long
    var cmp = util.eqArrayShallowNoOrder;

    it('works with empties', () => {
      expect(cmp([], [])).to.be.true;
      expect(cmp([], [1])).to.be.false;
      expect(cmp([], ['tetet'])).to.be.false;
    });
    it('works with single ints', () => {
      expect(cmp([1], [1])).to.be.true;
      expect(cmp([1], [2])).to.be.false;
    });
    it('works with many in order ints', () => {
      var a1 = [12,3,4,2212,-11];
      var a2 = ramda.clone(a1);
      var b1 = [];
      var c1 = [2, 232];
      expect(cmp(a1, a2)).to.be.true;
      expect(cmp(a1, b1)).to.be.false;
      expect(cmp(a1, c1)).to.be.false;
    });
    it('works with strings', () => {
      var a1 = ['1', 'something', 'fishsticks'];
      var a2 = ramda.clone(a1);
      var b1 = ['suckers'];
      expect(cmp(a1, a2)).to.be.true;
      expect(cmp(a1, b1)).to.be.false;
    });
    it('works out of order', () => {
      var a1 = ['1', 'something', 'fishsticks'];
      var a2 = ['something', '1', 'fishsticks'];
      var a3 = ['fishsticks', 'something', '1'];
      expect(cmp(a1, a2)).to.be.true;
      expect(cmp(a1, a3)).to.be.true;
    });
    it('works mixed', () => {
      var a1 = ['1', 2, 'something', 'fishsticks', 800];
      var a2 = [800, 2, 'something', '1', 'fishsticks'];
      var a3 = ['fishsticks', 800, 'something', '1', 2];
      expect(cmp(a1, a2)).to.be.true;
      expect(cmp(a1, a3)).to.be.true;
    });
  });
});
